<?php


namespace App\Services;


use App\Models\Comment;
use App\Repositories\CommentRepository;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

class CommentService {

    private CommentRepository $commentRepository;

    public function __construct(
        CommentRepository $commentRepository
    ) {
        $this->commentRepository = $commentRepository;
    }

    public function save(array $data): Comment {
        return $this->commentRepository
            ->save($data);
    }

    public function all(): LengthAwarePaginator {
        return $this->commentRepository
            ->all();
    }

    public function delete(string $id): bool {
        return $this->commentRepository
            ->delete($id);
    }

}

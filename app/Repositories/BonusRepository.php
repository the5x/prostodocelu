<?php


namespace App\Repositories;


use App\Interfaces\BonusRepositoryInterface;
use App\Models\Bonus;
use Illuminate\Database\Eloquent\Collection;

class BonusRepository implements BonusRepositoryInterface {

    private Bonus $bonusModel;

    public function __construct(
        Bonus $bonusModel
    ) {
        $this->bonusModel = $bonusModel;
    }

    public function create(array $data): Bonus {
        return $this->bonusModel
            ->create($data);
    }

    public function show(string $id): Bonus {
        return $this->bonusModel
            ->findOrFail($id);
    }

    public function update(array $data, string $id): bool {
        return $this->bonusModel
            ->where('id', $id)
            ->update($data);
    }

    public function all(): Collection {
        return $this->bonusModel
            ->orderBy('created_at')
            ->get();
    }

    public function delete(string $id): bool {
        return $this->bonusModel
            ->where('id', $id)
            ->delete();
    }

}

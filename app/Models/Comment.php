<?php

namespace App\Models;

use App\Traits\UuidTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use \Illuminate\Database\Eloquent\Relations\BelongsTo;

class Comment extends Model {

    use HasFactory, UuidTrait;

    public $timestamps = false;
    protected $table = 'comments';
    protected $fillable = ['profile_id', 'user_id', 'day_id', 'description'];

    public function user(): BelongsTo {
        return $this->belongsTo(User::class);
    }

    public function day(): BelongsTo {
        return $this->belongsTo(Day::class);
    }

}

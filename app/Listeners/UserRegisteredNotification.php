<?php

namespace App\Listeners;

use App\Events\UserRegisteredEvent;
use App\Mail\UserRegisteredMail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Mail;

class UserRegisteredNotification {
    public function handle(UserRegisteredEvent $event) {
        Mail::to($event->user->email)
            ->send(new UserRegisteredMail($event->verificationCode));
    }
}

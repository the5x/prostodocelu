<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ProfileUpdateRequest extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'first_name' => 'required|string|max:190',
            'last_name' => 'required|string|max:190',
            'weight' => 'required|integer|gt:0|lt:300',
            'height' => 'required|integer|gt:0|lt:250',
            'age' => 'required|integer|gt:0|lt:100',
            'sex' => ['required', Rule::in(['M', 'W'])],
            'photo' => 'nullable|image|mimes:jpeg,png,jpg,gif|max:512',
            'purpose' => 'required|max:190',
        ];
    }

}

<?php


namespace App\Transformers;


use App\Services\GenerateVerifyCodeService;
use App\Services\HashPasswordService;

class UserTransformer {

    private HashPasswordService $hashService;
    private GenerateVerifyCodeService $verifyService;

    public function __construct(
        HashPasswordService $hashService,
        GenerateVerifyCodeService $verifyService
    ) {
        $this->hashService = $hashService;
        $this->verifyService = $verifyService;
    }

    public function transform(array $data): array {
        ['email' => $email, 'password' => $password] = $data;

        return [
            'email' => $email,
            'password' => $this->hashService->hashPassword($password),
            'email_verify_code' => $this->verifyService->generateCode(),
        ];
    }

}

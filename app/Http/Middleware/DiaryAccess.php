<?php

namespace App\Http\Middleware;

use App\Models\Diary;
use Closure;
use Illuminate\Http\Request;

class DiaryAccess {

    public function handle(Request $request, Closure $next) {
        $routeID = $request->route('diary');
        $diary = Diary::findOrFail($routeID);

        if (auth()->check() && (auth()->user()->id === $diary->user_id || auth()->user()->is_admin)) {
            return $next($request);
        }

        abort(404);
    }

}

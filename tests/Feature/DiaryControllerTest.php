<?php

namespace Tests\Feature;

use App\Models\Diary;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class DiaryControllerTest extends TestCase {

    use RefreshDatabase;

    private User $user;
    private Diary $diary;

    protected function setUp(): void {
        parent::setUp(); // TODO: Change the autogenerated stub

        $this->user = User::factory()->create();
        $this->diary = Diary::factory(['user_id' => $this->user])->create();
    }

    public function getAuthenticatedUser(): void {
        $this->actingAs($this->user);
    }

    public function fillEmptyData(): array {
        return ['description' => ''];
    }

    public function test_make_redirect_if_unauthorized_user_tries_create_diary(): void {
        $this->get(route('diary.create'))
            ->assertStatus(302)
            ->assertRedirect(route('login'));
    }

    public function test_make_redirect_if_unauthorized_user_tries_edit_diary(): void {
        $this->get(route('diary.edit', ['diary' => $this->diary->id]))
            ->assertStatus(302)
            ->assertRedirect(route('login'));
    }

    public function test_make_redirect_if_unauthorized_user_tries_delete_diary(): void {
        $this->get(route('diary.delete', ['diary' => $this->diary->id]))
            ->assertStatus(302)
            ->assertRedirect(route('login'));
    }

    public function test_show_diary_create_page_if_user_logged(): void {
        $this->getAuthenticatedUser();

        $this->get(route('diary.create'))
            ->assertOk()
            ->assertViewIs('diary.create');
    }

    public function test_save_empty_fields(): void {
        $this->getAuthenticatedUser();

        $this->post(route('diary.save'), $this->fillEmptyData())
            ->assertSessionHasErrors([
                'description' => 'The description field is required.'
            ]);
    }

    public function test_create_and_save_diary_with_data(): void {
        $this->getAuthenticatedUser();

        $this->post(route('diary.save'), $this->diary->toArray());
        $this->get(route('profile.show', ['user' => $this->user->id]))
            ->assertOk()
            ->assertSeeText($this->diary->description)
            ->assertViewIs('profile.show');

        $this->assertDatabaseHas('diaries', $this->diary->toArray());
    }

    public function test_show_edit_page_with_data(): void {
        $this->getAuthenticatedUser();

        $this->get(route('diary.edit', ['diary' => $this->diary->id]))
            ->assertOk()
            ->assertSeeText($this->diary->description)
            ->assertViewIs('diary.edit');
    }

    public function test_update_diary_with_empty_fields(): void {
        $this->getAuthenticatedUser();

        $this->put(route('diary.update', ['diary' => $this->diary->id]), $this->fillEmptyData())
            ->assertSessionHasErrors([
                'description' => 'The description field is required.'
            ]);
    }

    public function test_update_diary_with_data(): void {
        $this->getAuthenticatedUser();

        $this->put(route('diary.update', ['diary' => $this->diary->id]), $this->diary->toArray());
        $this->get(route('profile.show', ['user' => $this->user->id]))
            ->assertOk()
            ->assertSeeText($this->diary->description)
            ->assertViewIs('profile.show');

        $this->assertDatabaseHas('diaries', $this->diary->toArray());
    }

    public function test_delete_diary_entry(): void {
        $this->getAuthenticatedUser();

        $this->delete(route('diary.delete', ['diary' => $this->diary->id]));
        $this->get(route('profile.show', ['user' => $this->user->id]))
            ->assertOk()
            ->assertDontSeeText($this->diary->description)
            ->assertViewIs('profile.show');

        $this->assertDeleted('diaries', $this->diary->toArray());
        $this->assertDatabaseCount('diaries', 0);
        $this->assertDatabaseMissing('diaries', $this->diary->toArray());
    }

}

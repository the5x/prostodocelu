<?php

namespace App\Models;

use App\Traits\UuidTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Diary extends Model {

    use HasFactory, UuidTrait;

    public $timestamps = false;
    protected $table = 'diaries';
    protected $fillable = ['description', 'created_at', 'user_id'];

}

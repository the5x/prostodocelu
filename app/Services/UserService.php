<?php


namespace App\Services;

use App\Events\UserRegisteredEvent;
use App\Models\User;
use App\Repositories\UserRepository;
use App\Transformers\UserTransformer;
use Mail;

class UserService {

    private UserRepository $userRepository;
    private UploadImageService $uploadService;
    private UserTransformer $userTransform;

    public function __construct(
        UserRepository $userRepository,
        UploadImageService $uploadService,
        UserTransformer $userTransform
    ) {
        $this->userRepository = $userRepository;
        $this->uploadService = $uploadService;
        $this->userTransform = $userTransform;
    }

    public function register($data): void {
        $transformData = $this->userTransform->transform($data);
        $user = $this->userRepository->create($transformData);

        event(new UserRegisteredEvent($user, $user['email_verify_code']));
    }

    public function findById(string $id): User {
        return $this->userRepository
            ->findById($id);
    }

    public function update(array $data, string $id): void {
        $this->userRepository->update($data, $id);

        if (isset($data['photo'])) {
            $fileName = $this->uploadService->savePhotoToDirectory($data['photo']);
            $this->userRepository->update(['photo' => $fileName], $id);
        }
    }

}

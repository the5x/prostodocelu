<?php


namespace App\Interfaces;


use App\Models\Day;
use Illuminate\Database\Eloquent\Collection;

interface DayRepositoryInterface {

    public function create(array $data): Day;

    public function findDayForCurrentUser(string $profileId, string $dayId);

    public function show(string $dayId): Day;

    public function all(): Collection;

    public function update(array $data, string $id): bool;

    public function delete(string $id): bool;

}

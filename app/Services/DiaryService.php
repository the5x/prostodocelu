<?php


namespace App\Services;


use App\Models\Diary;
use App\Repositories\DiaryRepository;
use App\Transformers\DiaryTransformer;

class DiaryService {

    private DiaryRepository $diaryRepository;
    private DiaryTransformer $diaryTransform;

    public function __construct(
        DiaryRepository $diaryRepository,
        DiaryTransformer $diaryTransform
    ) {
        $this->diaryRepository = $diaryRepository;
        $this->diaryTransform = $diaryTransform;
    }

    public function create(array $data): Diary {
        $diaryData = $this->diaryTransform->transform($data);

        return $this->diaryRepository
            ->create($diaryData);
    }

    public function findById(string $id): Diary {
        return $this->diaryRepository
            ->findById($id);
    }

    public function update(array $data, string $id): bool {
        return $this->diaryRepository
            ->update($data, $id);
    }

    public function delete(string $id): bool {
        return $this->diaryRepository
            ->delete($id);
    }

}

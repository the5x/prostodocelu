<form class="search" action="{{ route('search') }}" method="GET">
    <input placeholder="По имени, фамилии, или email" class="search__input" type="text" name="s"/>
    <button class="btn__ico" type="submit">
        <img src="{{ URL::to('/assets/images/search.svg') }}" alt="">
    </button>
</form>


<div class="thead">
    <div class="thead__column">Инициалы</div>
    <div class="thead__column">Email</div>
    <div class="thead__column">Текущий тарифный план</div>
    <div class="thead__column">Управление доступом</div>
    <div class="thead__column">Удаление</div>
</div>

@foreach($users as $user)
    <div class="control">
        <div class="control__column">
            <a class="control__link"
               href="{{ route('profile.show', ['user' => $user->id]) }}">{{ $user->first_name }} {{ $user->last_name }}</a>
        </div>
        <div class="control__column">
            {{ $user->email }}
        </div>
        <div class="control__column">
            <form
                action="{{ route('profile.rate.update', ['user' => $user->id]) }}"
                method="POST">
                @method('PUT')
                @csrf

                <select class="control__select" name="rate">
                    @foreach($rates as $rate)
                        <option name="rate" class="control__option"
                                {{ $user->rate_id === $rate->id ? 'selected' : '' }} value="{{ $rate->id }}">{{ $rate->title }}</option>
                    @endforeach
                </select>

                <button class="btn__ico" type="submit">
                    <img src="{{ URL::to('/assets/images/edit.svg') }}" alt="">
                </button>
            </form>
        </div>

        <div class="control__column">
            @if($user->is_banned)
                <form action="{{ route('profile.unban', ['user' => $user->id]) }}" method="POST">
                    @method('PUT')
                    @csrf

                    <button class="btn__ico" type="submit">
                        <img src="{{ URL::to('/assets/images/eye-crossed.svg') }}" alt="">
                    </button>
                </form>
            @else
                <form action="{{ route('profile.ban', ['user' => $user->id]) }}" method="POST">
                    @method('PUT')
                    @csrf

                    <button class="btn__ico" type="submit">
                        <img src="{{ URL::to('/assets/images/eye.svg') }}" alt="">
                    </button>
                </form>
            @endif
        </div>

        <div class="control__column">
            <form onclick="return confirm('Удаление пользователя без возможности восстановления. Подтверждаете удаление?')" action="{{ route('profile.delete', ['user' => $user->id]) }}" method="POST">
                @method('DELETE')
                @csrf

                <button class="btn__ico" type="submit">
                    <img src="{{ URL::to('/assets/images/bin.svg') }}" alt="">
                </button>
            </form>
        </div>
    </div>
@endforeach

{{ $users->onEachSide(1)->links() }}

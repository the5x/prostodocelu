@guest
    <li class="footer__lst-item">
        <a class="footer__list-item-link" href="{{ route('login') }}">Войти</a>
    </li>
@endguest
@auth
    <li class="footer__lst-item">
        <a class="footer__list-item-link" href="{{ route('rate.index')  }}">Тарифы</a>
    </li>
    <li class="footer__lst-item">
        <a class="footer__list-item-link"
           href="{{ route('training.start', ['user' => auth()->user()->id]) }}">Программа</a>
    </li>
    <li class="footer__lst-item">
        <a class="footer__list-item-link" href="{{ route('bonus.all') }}">Бонусы</a>
    </li>

    <li class="footer__lst-item">
        <a class="footer__list-item-link" href="{{ route('profile.show', ['user' => auth()->user()->id]) }}">Профиль</a>
    </li>
@endauth

<li class="footer__lst-item">
    <a class="footer__list-item-link" href="{{ route('user-agreement') }}">Пользовательское соглашение</a>
</li>

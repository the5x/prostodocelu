<?php

namespace App\Models;

use App\Traits\UuidTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Rate extends Model {

    use HasFactory, UuidTrait;

    public $timestamps = false;
    protected $table = 'rates';
    protected $fillable = ['price', 'title', 'rate_id', 'subtitle', 'advantage'];
}

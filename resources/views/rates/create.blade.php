@extends('layouts.child')

@section('keywords', '')
@section('description', '')

@section('content')
    <div class="admin">
        <section class="wrapper">
            <h3 class="section__title section__subtitle">Создать тариф</h3>

            <form action="{{ route('rate.save') }}" method="POST">
                @csrf

                <div class="form__table">
                    <label class="form__label" for="price">Цена</label>
                    @error('price')<div class="error">{{ $message }}</div>@enderror
                    <input value="{{ old('price') }}" type="number" class="form__input" name="price" />

                    <label class="form__label" for="title">Название</label>
                    @error('title')<div class="error">{{ $message }}</div>@enderror
                    <input value="{{ old('title') }}" type="text" class="form__input" name="title" />

                    <label class="form__label" for="subtitle">Подзаголовок</label>
                    @error('subtitle')<div class="error">{{ $message }}</div>@enderror
                    <input value="{{ old('subtitle') }}" type="text" class="form__input" name="subtitle" />

                    <label class="form__label" for="advantage">Описание преимуществ</label>
                    @error('advantage')<div class="error">{{ $message }}</div>@enderror
                    <textarea class="form__input form__input_textarea form__input_border" name="advantage"
                              cols="30" rows="10">{{ old('advantage') }}</textarea>
                </div>
                <br><br>
                <button class="motivation__btn pulse__btn" type="submit">Создать тариф</button>
            </form>

        </section>
    </div>
@stop

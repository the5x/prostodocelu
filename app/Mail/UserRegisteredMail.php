<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class UserRegisteredMail extends Mailable {
    use Queueable, SerializesModels;

    private $verificationCode;

    public function __construct($verificationCode) {
        $this->verificationCode = $verificationCode;
    }

    public function build() {
        return $this->from(config('mail.from.address'))
            ->view('emails.registered', ['verificationCode' => $this->verificationCode]);
    }
}

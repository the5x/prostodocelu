<style>
    .mail__wrapper {
        height: 100%;
        padding: 20px;
        background-color: #0a0a0a;
        font-family: sans-serif;
    }

    .mail__box {
        position: absolute;
        max-width: 700px;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
        text-align: center;
    }

    .mail__title {
        display: block;
        margin: 0 auto 30px;
        font-weight: bold;
        font-size: 24px;
        text-transform: uppercase;
        color: #cbd5e0;
    }

    .mail__text {
        display: block;
        margin-bottom: 30px;
        font-size: 16px;
        line-height: 24px;
        color: #cbd5e0;
        text-align: center;
    }

    .mail__button {
        display: inline-block;
        padding: 10px 20px;
        text-align: center;
        text-transform: uppercase;
        font-weight: bold;
        font-size: 14px;
        color: #cbd5e0;
        background-color: #e60000;
        text-decoration: none;
    }
</style>

<div class="mail__wrapper">
    <div class="mail__box">
        <H1 class="mail__title">Верификация email</H1>
        <p class="mail__text">Сущность и концепция маркетинговой программы тормозит ролевой SWOT-анализ. Системный анализ индуктивно программирует эмпирический формирование имиджа, расширяя долю рынка.</p>
        <a class="mail__button" href="{{ route('verification', ['code' => $verificationCode])  }}" target="_blank">Верифицировать email</a>
    </div>
</div>





<?php

namespace App\Http\Controllers;

use App\Http\Requests\BonusRequest;
use App\Services\BonusService;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;

class BonusController extends Controller {

    private BonusService $bonusService;

    public function __construct(
        BonusService $bonusService
    ) {
        $this->bonusService = $bonusService;
    }

    public function index(): View {
        $bonuses = $this->bonusService->all();
        return view('bonuses.all', compact('bonuses'));
    }

    public function create(): View {
        return view('bonuses.create');
    }

    public function save(BonusRequest $request): RedirectResponse {
        $this->bonusService->create($request->validated());
        return redirect()->route('bonus.all');
    }

    public function show(string $id): View {
        $bonus = $this->bonusService->show($id);
        return view('bonuses.bonus', compact('bonus'));
    }

    public function edit(string $id): View {
        $bonus = $this->bonusService->show($id);
        return view('bonuses.edit', compact('bonus'));
    }

    public function update(BonusRequest $request, string $id): RedirectResponse {
        $this->bonusService->update($request->validated(), $id);
        return redirect()->route('bonus.all');
    }

    public function all(): View {
        $bonuses = $this->bonusService->all();
        return view('bonuses.all', compact('bonuses'));
    }

    public function delete(string $id): RedirectResponse {
        $this->bonusService->delete($id);
        return redirect()->route('bonus.all');
    }

}

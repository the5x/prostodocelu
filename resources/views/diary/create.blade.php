@extends('layouts.child')

@section('keywords', '')
@section('description', '')

@section('content')
    <div class="admin">
        <section class="wrapper">
            <h3 class="section__title section__subtitle">Создать запись</h3>

            <form action="{{ route('diary.save') }}" method="POST">
                @csrf

                <div class="form__table">
                    <label class="form__label" for="description">Дневник</label>
                    @error('description')
                    <div class="error">{{ $message }}</div>@enderror
                    <textarea class="form__input form__input_textarea form__input_border" name="description"
                              cols="30" rows="10">{{ old('description') }}</textarea>
                </div>
                <br><br>
                <button class="motivation__btn pulse__btn" type="submit">Создать запись</button>
            </form>

        </section>
    </div>
@stop

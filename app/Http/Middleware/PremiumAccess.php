<?php

namespace App\Http\Middleware;

use App\Models\Rate;
use Closure;
use Illuminate\Http\Request;

class PremiumAccess {

    private const ACCESS_DESCRIPTION = 'access only for premium accounts';

    public function getUserRateId() {
        return auth()->user()->rate_id;
    }

    public function getFreeRateId() {
        return Rate::orderBy('price', 'ASC')->first()->id;
    }

    public function getAdminStatus() {
        return auth()->user()->is_admin;
    }

    public function handle(Request $request, Closure $next) {
        if ((auth()->check() && ($this->getUserRateId() !== $this->getFreeRateId() &&  !is_null($this->getUserRateId()))) ||
            $this->getAdminStatus()
        ) {
            return $next($request);
        }

        abort(403, self::ACCESS_DESCRIPTION);
    }

}

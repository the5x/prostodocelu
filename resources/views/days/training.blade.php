@extends('layouts.child')

@section('keywords', '')
@section('description', '')

@section('content')
    @if(!$premiumAccount)
        @include('includes.attention')
    @endif
    <section class="days">
        <section class="wrapper">
            <div class="days__box">
                <div class="days__column days__column_mb">
                    <h1 class="section__title section__title_mt">{{ $day->title }}</h1>

                    @if(auth()->user()->is_admin)
                        <form action="{{ route('training.open', ['user' => $user->id, 'day' => $day->id]) }}"
                              method="post">
                            @csrf
                            <button type="submit" class="motivation__btn pulse__btn">Открыть следующий день →</button>
                        </form>
                    @endif

                    <div class="bonus__video">
                        <iframe class="bonuses__iframe" src="https://www.youtube.com/embed/{{ $day->video_first }}"
                                frameborder="0"
                                allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                                allowfullscreen></iframe>
                    </div>

                    <div class="days__description">
                        {{ $day->description }}
                    </div>

                    <div class="bonus__video">
                        <iframe class="bonuses__iframe" src="https://www.youtube.com/embed/{{$day->video_second }}"
                                frameborder="0"
                                allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                                allowfullscreen></iframe>
                    </div>

                    <h4 class="section__subtitle">Комментарии</h4>

                    @foreach($day->comments as $comment)
                        @include('includes.comments')
                    @endforeach

                    <form action="{{ route('comment.save', ['user' => $user->id, 'day' => $day->id])  }}" method="POST"
                          enctype="multipart/form-data">
                        @csrf

                        <textarea class="" id="editor" name="description"></textarea>
                        <br/><br/>

                        <script src="{{ asset('assets/js/ckeditor/ckeditor.js') }}"></script>
                        <script>
                            CKEDITOR.replace('editor', {
                                filebrowserUploadUrl: "{{ route('upload', ['_token' => csrf_token() ]) }}",
                                filebrowserUploadMethod: 'form',
                            });
                        </script>

                        <button type="submit" class="motivation__btn pulse__btn">Добавить комментарий →</button>
                    </form>
                </div>
                <div class="days__column days__column_mw">
                    @include('includes.sidebar')
                </div>
            </div>
        </section>
    </section>
@stop

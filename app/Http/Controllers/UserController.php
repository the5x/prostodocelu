<?php


namespace App\Http\Controllers;

use App\Http\Requests\ProfileUpdateRequest;
use App\Models\Rate;
use App\Models\User;
use App\Services\UserService;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class UserController extends Controller {

    private UserService $userService;

    public function __construct(
        UserService $userService
    ) {
        $this->userService = $userService;
    }

    public function edit(string $id): View {
        $user = $this->userService->findById($id);
        return view('profile.edit', compact('user'));
    }

    public function show(string $id): View {
        $user = $this->userService->findById($id);
        return view('profile.show', compact('user'));
    }

    public function update(ProfileUpdateRequest $request, string $id): RedirectResponse {
        $this->userService->update($request->validated(), $id);
        return redirect()->route('profile.show', ['user' => $id]);
    }

    public function all(Request $request) {
        $rates = Rate::orderBy('price', 'ASC')->get();
        $users = User::with('rate')->orderBy('first_name', 'ASC')->paginate(20);

        return view('profile.all', compact('rates', 'users'));
    }

    public function delete($id) {
        User::where('id', $id)->delete();

        return redirect()->route('profile.all');
    }

}

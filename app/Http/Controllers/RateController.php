<?php

namespace App\Http\Controllers;

use App\Http\Requests\RateRequest;
use App\Models\User;
use App\Services\RateService;
use App\Services\UserService;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RateController extends Controller {

    private RateService $rateService;
    private UserService $userService;

    public function __construct(
        RateService $rateService,
        UserService $userService
    ) {
        $this->rateService = $rateService;
        $this->userService = $userService;

        $this->middleware('auth');
//        $this->middleware('admin.access')->except(['index']);
    }

    public function index(): View {
        $rates = $this->rateService->all();
        return view('rates.all', compact('rates'));
    }

    public function create(): View {
        return view('rates.create');
    }

    public function save(RateRequest $request): RedirectResponse {
        $this->rateService->create($request->validated());
        return redirect()->route('rate.index');
    }

    public function edit(string $id): View {
        $rate = $this->rateService->findById($id);
        return view('rates.edit', compact('rate'));
    }

    public function update(RateRequest $request, string $id): RedirectResponse {
        $this->rateService->update($id, $request->validated());
        return redirect()->route('rate.index');
    }

    public function delete(string $id): RedirectResponse {
        $this->rateService->delete($id);
        return redirect()->route('rate.index');
    }

    public function updateRateFromAdmin(Request $request, $userId) {
        User::where('id', $userId)
            ->update([
                'rate_id' => $request->rate,
            ]);

        return redirect()->back();
    }

    public function chooseAnotherRate(string $id): RedirectResponse {
        $rate = $this->rateService->findById($id);
        $currentUser = $this->userService->findById(Auth::id());
        $this->rateService->chooseAnotherRate($currentUser->id, $rate->id);

        return redirect()->back();
    }

}

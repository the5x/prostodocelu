@extends('layouts.child')

@section('keywords', '')
@section('description', '')

@section('content')
    <div class="admin">
        <section class="wrapper">
            <h3 class="section__title section__subtitle">Редактировать профиль</h3>

            <form action="{{ route('profile.update', ['user' => $user->id ]) }}" method="POST"
                  enctype="multipart/form-data">
                @method('PUT')
                @csrf

                <div class="form__table">
                    @error('photo')
                    <div class="error">{{ $message }}</div>@enderror
                    <label class="form__label" for="photo">Фото</label>
                    <input class="form__input form__input_file form__input_border-none" type="file" name="photo">

                    <label class="form__label" for="first_name">Имя</label>
                    @error('first_name')
                    <div class="error">{{ $message }}</div>@enderror
                    <input class="form__input" type="text" name="first_name"
                           value="{{ old('first_name') ?: $user->first_name }}">

                    <label class="form__label" for="last_name">Фамилия</label>
                    @error('last_name')
                    <div class="error">{{ $message }}</div>@enderror
                    <input class="form__input" type="text" name="last_name"
                           value="{{ old('last_name') ?: $user->last_name }}">

                    <label class="form__label" for="weight">Вес</label>
                    @error('weight')
                    <div class="error">{{ $message }}</div>@enderror
                    <input class="form__input" type="number" name="weight"
                           value="{{ old('weight') ?: $user->weight }}">

                    <label class="form__label" for="height">Рост</label>
                    @error('height')
                    <div class="error">{{ $message }}</div>@enderror
                    <input class="form__input" type="number" name="height"
                           value="{{ old('height') ?: $user->height }}">

                    <label class="form__label" for="age">Возраст</label>
                    @error('age')
                    <div class="error">{{ $message }}</div>@enderror
                    <input class="form__input" type="number" name="age"
                           value="{{ old('age') ?: $user->age }}">

                    <label class="form__label" for="sex">Пол</label>
                    @error('sex')
                    <div class="error">{{ $message }}</div>@enderror
                    <select class="form__input form__input_border-none" name="sex" id="sex">
                        <option class="form__input form__input_border-none" value="M">M</option>
                        <option class="form__input form__input_border-none" value="W">W</option>
                    </select>

                    <label class="form__label" for="purpose">Цель</label>
                    @error('purpose')
                    <div class="error">{{ $message }}</div>@enderror
                    <textarea class="form__input form__input_textarea form__input_border" name="purpose" cols="30"
                              rows="10">{{ old('purpose') ?: $user->purpose }}</textarea>
                </div>
                <br><br>
                <button class="motivation__btn pulse__btn" type="submit">Обновить профиль</button>
            </form>

            @if(Auth::check() && Auth::user()->is_admin)

                @if(empty($user->is_banned))
                    <form action="{{ route('profile.ban', ['user' => $user->id])  }}" method="POST">
                        @method('PUT')
                        @csrf

                        <button class="edit__button" type="submit">Заблокировать</button>
                    </form>
                @else
                    <form action="{{ route('profile.unban', ['user' => $user->id])  }}" method="POST">
                        @method('PUT')
                        @csrf

                        <button class="edit__button" type="submit">Paзблокировать</button>
                    </form>
                @endif

            @endif

        </section>
    </div>
@stop

<?php

namespace App\Repositories;

use App\Interfaces\UserBanInterface;
use App\Interfaces\UserRepositoryInterface;
use App\Models\User;

class UserRepository implements UserRepositoryInterface, UserBanInterface {

    private const USER_IS_BANNED = true;
    private User $userModel;

    public function __construct(
        User $userModel
    ) {
        $this->userModel = $userModel;
    }

    public function create(array $data): User {
        return $this->userModel
            ->create($data);
    }

    public function findById(string $id): User {
        return $this->userModel
            ->findOrFail($id);
    }

    public function update(array $data, string $id): bool {
        return $this->userModel
            ->where('id', $id)
            ->update($data);
    }

    public function ban(string $id): bool {
        return $this->userModel
            ->where('id', $id)
            ->update(['is_banned' => self::USER_IS_BANNED]);
    }

    public function unban(string $id): bool {
        return $this->userModel
            ->where('id', $id)
            ->update(['is_banned' => !self::USER_IS_BANNED]);
    }

}

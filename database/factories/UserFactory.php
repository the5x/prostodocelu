<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class UserFactory extends Factory {

    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = User::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition() {
        return [
            'first_name' => $this->faker->firstName('male'),
            'last_name' => $this->faker->lastName(),
            'email' => $this->faker->unique()->safeEmail,
            'password' => '$2y$10$0FftgrcmqS4Qix9Ikj07Pu1sP1.rgXcxnaMk4X8mhagg2jmQnBlLy',
            'weight' => $this->faker->numberBetween(60, 90),
            'height' => $this->faker->numberBetween(170, 190),
            'age' => $this->faker->numberBetween(20, 90),
            'sex' => $this->faker->randomElement(['M', 'W']),
            'purpose' => $this->faker->sentence($nbWords = 10, $variableNbWords = true),
            'is_banned' => false,
            'is_verified' => 1,
        ];
    }

}

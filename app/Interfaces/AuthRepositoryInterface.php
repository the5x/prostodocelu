<?php

namespace App\Interfaces;

use App\Http\Requests\ForgotPasswordRequest;
use App\Http\Requests\ResetPasswordRequest;
use App\Models\PasswordReset;
use App\Models\User;

interface AuthRepositoryInterface {

    public function verifyEmail(string $code): bool;

    public function generateToken(array $data): bool;

    public function canUpdatePassword(array $data): ?PasswordReset;

    public function updatePassword(array $data): bool;

    public function removeToken(string $email): bool;

}

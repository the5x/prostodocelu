@extends('layouts.child')

@section('keywords', '')
@section('description', '')

@section('content')
    <section class="bonuses">
        <section class="wrapper">
            @if(auth()->check() && auth()->user()->is_admin)
                <a href="{{ route('bonus.edit', ['bonus' => $bonus->id]) }}">
                    <img class="profile__ico-edit"
                         src="{{ URL::to('/assets/images/ico-edit.svg') }}"
                         alt=""></a>
            @endif
            <h1 class="section__title">{{ $bonus->title }}</h1>
            <p class="bonuses__text">{{ $bonus->description }}</p>

            <div class="bonus__video">
                <iframe class="bonuses__iframe" src="https://www.youtube.com/embed/{{ $bonus->video }}"
                        frameborder="0"
                        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                        allowfullscreen></iframe>
            </div>
        </section>
    </section>
@stop

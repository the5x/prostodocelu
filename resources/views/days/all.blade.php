@extends('layouts.child')

@section('keywords', '')
@section('description', '')

@section('content')
    <section class="days">
        <section class="wrapper">
            <a href="{{ route('day.create')  }}">
                <img class="profile__ico-edit"
                     src="{{ URL::to('/assets/images/ico-edit.svg') }}" alt="">
            </a>
            <h1 class="section__title">Тренировочные дни</h1>
            <p class="bonuses__text">В соответствии с законом Ципфа, начальная стадия проведения исследования
                синхронизирует тактический имидж предприятия. Узнавание бренда ускоряет рекламный макет. Диверсификация
                бизнеса, суммируя приведенные примеры, экономит типичный традиционный канал.</p>

            <div class="bonuses__box">
                @isset($days)
                    @foreach($days as $day)
                        <div class="bonuses__column">
                            <img class="bonuses__iframe"
                                 src="https://img.youtube.com/vi/{{ $day->video_first }}/maxresdefault.jpg"
                                 alt="{{ $day->title }}">
                            <a class="bonuses__iframe-link"
                               href="{{ route('day.show', ['day' => $day->id])  }}">{{ Str::limit($day->title, 50, '...') }}</a>
                        </div>
                    @endforeach
                @endisset
            </div>

        </section>
    </section>
@stop

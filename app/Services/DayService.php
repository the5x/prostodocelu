<?php


namespace App\Services;


use App\Models\Day;
use App\Repositories\DayRepository;
use Illuminate\Database\Eloquent\Collection;

class DayService {

    private DayRepository $dayRepository;

    public function __construct(
        DayRepository $dayRepository
    ) {
        $this->dayRepository = $dayRepository;
    }

    public function all(): Collection {
        return $this->dayRepository
            ->all();
    }

    public function findDayForCurrentUser(string $profileId, string $dayId): Day {
        return $this->dayRepository
            ->findDayForCurrentUser($profileId, $dayId);
    }

    public function show(string $dayId): Day {
        return $this->dayRepository
            ->show($dayId);
    }


    public function create(array $data): Day {
        return $this->dayRepository
            ->create($data);
    }

    public function update(array $data, string $id): bool {
        return $this->dayRepository
            ->update($data, $id);
    }

    public function delete(string $id): bool {
        return $this->dayRepository
            ->delete($id);
    }

    public function unlockNextDay(Day $previousDay): ?Day {
        return $this->dayRepository
            ->unlockNextDay($previousDay);
    }

    public function getFirstDay(): ?Day {
        return $this->dayRepository
            ->getFirstDay();
    }

}

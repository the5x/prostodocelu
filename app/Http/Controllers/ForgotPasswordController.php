<?php

namespace App\Http\Controllers;

use App\Http\Requests\ForgotPasswordRequest;
use App\Services\AuthService;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;

class ForgotPasswordController extends Controller {

    private AuthService $authService;

    public function __construct(
        AuthService $authService
    ) {
        $this->authService = $authService;

        $this->middleware('guest');
    }

    public function index(): View {
        return view('auth.forgot-password');
    }

    public function sendTokenToEmail(ForgotPasswordRequest $request): RedirectResponse {
        $this->authService->generateToken($request->validated());
        return redirect()->back();
    }

}

@extends('layouts.child')

@section('keywords', '')
@section('description', '')

@section('content')
    <section class="wrapper">
        <h3 class="section__title section__subtitle">Все комментарии</h3>

        @foreach($comments as $comment)
            <div class="comment__wrapper">
                <a class="comment__title"
                   href="{{ route('training.show', ['user' => $comment->user->id, 'day' => $comment->day->id])  }}">
                    {{ $comment->day->title }}
                </a>

                @include('includes.comments')
            </div>
        @endforeach

        {{ $comments->onEachSide(1)->links() }}
    </section>
@stop

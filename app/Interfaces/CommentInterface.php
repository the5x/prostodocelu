<?php


namespace App\Interfaces;


use App\Models\Comment;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

interface CommentInterface {

    public function save(array $data): Comment;

    public function all(): LengthAwarePaginator;

    public function delete(string $id): bool;

}

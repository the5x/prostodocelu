<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class BanControllerTest extends TestCase {

    use RefreshDatabase;

    private User $user;

    protected function setUp(): void {
        parent::setUp(); // TODO: Change the autogenerated stub

        $this->user = User::factory()->create();
    }

    public function getAuthenticatedUser(): void {
        $this->actingAs($this->user);
    }

    public function test_make_redirect_if_unauthorized_user_tries_show_edit_user_page(): void {
        $this->get(route('profile.edit', ['user' => $this->user->id]))
            ->assertStatus(302)
            ->assertRedirect('login');
    }

    public function test_user_ban(): void {
        $this->getAuthenticatedUser();

        $this->put(route('profile.ban', ['user' => $this->user->id]), ['is_banned' => true]);
        $this->get(route('profile.show', ['user' => $this->user->id]))
            ->assertOk()
            ->assertViewIs('profile.show')
            ->assertSeeText('banned');
    }

    public function test_user_unban(): void {
        $this->getAuthenticatedUser();

        $this->put(route('profile.unban', ['user' => $this->user->id]), ['is_banned' => false]);

        $this->get(route('profile.show', ['user' => $this->user->id]))
            ->assertOk()
            ->assertViewIs('profile.show')
            ->assertDontSeeText('banned');
    }

}

<?php


namespace App\Services;


use App\Repositories\UserRepository;

class BanService {

    private UserRepository $userRepository;

    public function __construct(
        UserRepository $userRepository
    ) {
        $this->userRepository = $userRepository;
    }

    public function ban(string $id): bool {
        return $this->userRepository
            ->ban($id);
    }

    public function unban(string $id): bool {
        return $this->userRepository
            ->unban($id);
    }

}

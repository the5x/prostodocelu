<?php


namespace App\Repositories;


use App\Interfaces\DayRepositoryInterface;
use App\Interfaces\TrainingInterface;
use App\Models\Day;
use Illuminate\Database\Eloquent\Collection;

class DayRepository implements DayRepositoryInterface, TrainingInterface {

    private Day $dayModel;

    public function __construct(
        Day $dayModel
    ) {
        $this->dayModel = $dayModel;
    }

    public function create(array $data): Day {
        return $this->dayModel
            ->create($data);
    }

    public function findDayForCurrentUser(string $profileId, string $dayId): Day {
        return $this->dayModel::with(['comments' => function ($query) use ($profileId) {
            return $query->where('profile_id', $profileId);
        }])
            ->findOrFail($dayId);
    }

    public function show(string $dayId): Day {
        return $this->dayModel
            ->findOrFail($dayId);
    }

    public function all(): Collection {
        return $this->dayModel
            ->orderBy('created_at', 'ASC')
            ->get();
    }

    public function update(array $data, string $id): bool {
        return $this->dayModel
            ->where('id', $id)
            ->update($data);
    }

    public function delete(string $id): bool {
        return $this->dayModel
            ->where('id', $id)
            ->delete();
    }

    public function unlockNextDay(Day $previousDay): ?Day {
        return $this->dayModel
            ->where('created_at', '>', $previousDay->created_at)
            ->orderBy('created_at', 'ASC')
            ->first();
    }

    public function getFirstDay(): ?Day {
        return $this->dayModel
            ->orderBy('created_at', 'ASC')
            ->first();
    }

}

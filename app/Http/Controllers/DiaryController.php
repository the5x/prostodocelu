<?php

namespace App\Http\Controllers;

use App\Http\Requests\DiaryRequest;
use App\Services\DiaryService;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;

class DiaryController extends Controller {

    private DiaryService $diaryService;

    public function __construct(
        DiaryService $diaryService
    ) {
        $this->diaryService = $diaryService;

        $this->middleware('auth');
        $this->middleware('diary.access')->except(['create', 'save']);
    }

    public function create(): View {
        return view('diary.create');
    }

    public function save(DiaryRequest $request): RedirectResponse {
        $this->diaryService->create($request->validated());
        return redirect()->route('profile.show', ['user' => auth()->user()->id]);
    }

    public function edit(string $id): View {
        $diary = $this->diaryService->findById($id);
        return view('diary.edit', compact('diary'));
    }

    public function update(DiaryRequest $request, string $id): RedirectResponse {
        $this->diaryService->update($request->validated(), $id);
        $userId = $this->diaryService->findById($id)->user_id;

        return redirect()->route('profile.show', ['user' => $userId]);
    }

    public function delete(string $id): RedirectResponse {
        $userId = $this->diaryService->findById($id)->user_id;
        $this->diaryService->delete($id);

        return redirect()->route('profile.show', ['user' => $userId]);
    }

}

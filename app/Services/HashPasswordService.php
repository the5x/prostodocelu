<?php

namespace App\Services;

use Illuminate\Support\Facades\Hash;

class HashPasswordService {

    public function hashPassword($password): string {
        return Hash::make($password);
    }

}

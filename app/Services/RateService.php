<?php


namespace App\Services;


use App\Models\Rate;
use App\Repositories\RateRepository;
use Illuminate\Database\Eloquent\Collection;

class RateService {

    private RateRepository $rateRepository;

    public function __construct(
        RateRepository $rateRepository
    ) {
        $this->rateRepository = $rateRepository;
    }

    public function create(array $data): Rate {
        return $this->rateRepository
            ->create($data);
    }

    public function findById(string $id): Rate {
        return $this->rateRepository
            ->findById($id);
    }

    public function update(string $id, array $data): bool {
        return $this->rateRepository
            ->update($id, $data);
    }

    public function all(): Collection {
        return $this->rateRepository
            ->all();
    }

    public function delete(string $id): bool {
        return $this->rateRepository
            ->delete($id);
    }

    public function chooseAnotherRate(string $userId, string $rateId): bool {
        return $this->rateRepository
            ->chooseAnotherRate($userId, $rateId);
    }

}

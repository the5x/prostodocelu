@extends('layouts.child')

@section('keywords', '')
@section('description', '')

@section('content')
    <section class="rates">
        <div class="wrapper">
            @if(auth()->user()->is_admin)
                <a href="{{ route('rate.create')  }}">
                    <img class="profile__ico-edit" src="{{ URL::to('/assets/images/ico-edit.svg') }}" alt="">
                </a>
            @endif

            <h1 class="section__title">Тарифы</h1>
            <p class="rates__description">В соответствии с законом Ципфа, начальная стадия проведения исследования
                синхронизирует тактический имидж предприятия. Узнавание бренда ускоряет рекламный макет. Диверсификация
                бизнеса, суммируя приведенные примеры, экономит типичный традиционный канал.</p>

            @isset($rates)
                @foreach($rates as $rate)
                    <div class="rates__box">
                        <div class="rates__column">

                            @if(auth()->user()->is_admin)
                                <a href="{{ route('rate.edit', ['rate' => $rate->id])  }}">
                                    <img class="profile__ico-edit" src="{{ URL::to('/assets/images/ico-edit.svg') }}"
                                         alt="">
                                </a>
                            @endif
                            <strong class="rates__number">{{ $rate->price  }}</strong>
                            <small class="rates__currency">zl</small>
                        </div>
                        <div class="rates__column">
                            <strong class="rates__title">{{ $rate->title }}</strong>
                            <small class="rates__subtitle">{{ $rate->subtitle }}</small>
                        </div>
                        <div class="rates__column">
                            <p class="rates__advantage">
                                {{ $rate->advantage }}
                            </p>

                            @if($rate->price > 0)
                                <form action="{{ route('rate.choose', ['rate' => $rate->id]) }}" method="POST">
                                    @method('PUT')
                                    @csrf

                                    <button type="submit" class="rates__button">Выбрать</button>
                                </form>
                            @endif
                        </div>
                    </div>

                @endforeach
            @endisset
        </div>
    </section>
@stop

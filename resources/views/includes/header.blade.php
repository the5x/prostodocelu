<header class="header-small">
    <div class="wrapper">
        <div class="header-small__box">
            <a href="{{ route('home')  }}"><img src="{{ URL::to('/assets/images/logo-small.png')  }}" alt=""></a>
            @include('includes.nav-child')
        </div>
    </div>
</header>

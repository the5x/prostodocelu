<nav class="header-small__nav">
    <ul class="header-small__list">
        @auth
            @if(auth()->user()->is_admin)
                <li class="header-small__item">
                    <a class="header-small__item-link" href="{{ route('profile.all') }}">Пользователи</a>
                </li>
                <li class="header-small__item">
                    <a class="header-small__item-link" href="{{ route('comment.all') }}">Комментарии</a>
                </li>
                <li class="header-small__item">
                    <a class="header-small__item-link" href="{{ route('day.all') }}">Дни</a>
                </li>
            @endif
            <li class="header-small__item">
                <a class="header-small__item-link" href="{{ route('rate.index') }}">Тарифы</a>
            </li>
            <li class="header-small__item">
                <a class="header-small__item-link" href="{{ route('training.start', ['user' => auth()->user()->id]) }}">Программа</a>
            </li>
            <li class="header-small__item">
                <a class="header-small__item-link" href="{{ route('bonus.all') }}">Бонусы</a>
            </li>
            <li class="header__list-item">
                <a class="header__list-item-link" href="{{ route('profile.show', ['user' => auth()->user()->id]) }}">Профиль</a>
            </li>
            <li class="header-small__item">
                <form action="{{ route('logout') }}" method="post">
                    @csrf
                    <button class="header__list-item-link">
                        <img src="{{ URL::to('/assets/images/ico-offline.png') }}" alt="">
                    </button>
                </form>
            </li>
        @endauth

        @guest
            <li class="header-small__item">
                <a class="header-small__item-link" href="{{ route('login.index') }}">
                    <img src="{{ URL::to('/assets/images/ico-offline.png')  }}" alt="">
                </a>
            </li>
        @endguest
    </ul>
</nav>

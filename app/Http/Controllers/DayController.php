<?php

namespace App\Http\Controllers;

use App\Http\Requests\DayRequest;
use App\Services\DayService;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;

class DayController extends Controller {

    private DayService $dayService;

    public function __construct(
        DayService $dayService
    ) {
        $this->dayService = $dayService;
    }

    public function create(): View {
        return view('days.create');
    }

    public function save(DayRequest $request): RedirectResponse {
        $this->dayService->create($request->validated());
        return redirect()->route('day.all');
    }

    public function show(string $id): View {
        $day = $this->dayService->show($id);
        return view('days.day', compact('day'));
    }

    public function all(): View {
        $days = $this->dayService->all();
        return view('days.all', compact('days'));
    }

    public function edit(string $id): View {
        $day = $this->dayService->show($id);
        return view('days.edit', compact('day'));
    }

    public function update(DayRequest $request, string $id): RedirectResponse {
        $this->dayService->update($request->validated(), $id);
        return redirect()->route('day.show', ['day' => $id]);
    }

    public function delete(string $id): RedirectResponse {
        $this->dayService->delete($id);
        return redirect()->route('day.all');
    }

}

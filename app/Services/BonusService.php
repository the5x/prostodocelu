<?php


namespace App\Services;


use App\Models\Bonus;
use App\Repositories\BonusRepository;
use Illuminate\Database\Eloquent\Collection;

class BonusService {

    private BonusRepository $bonusRepository;

    public function __construct(
        BonusRepository $bonusRepository
    ) {
        $this->bonusRepository = $bonusRepository;
    }

    public function create(array $data): Bonus {
        return $this->bonusRepository
            ->create($data);
    }

    public function show(string $id): Bonus {
        return $this->bonusRepository
            ->show($id);
    }

    public function update(array $data, string $id): bool {
        return $this->bonusRepository
            ->update($data, $id);
    }

    public function all(): Collection {
        return $this->bonusRepository
            ->all();
    }

    public function delete(string $id): bool {
        return $this->bonusRepository
            ->delete($id);
    }

}

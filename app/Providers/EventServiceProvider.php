<?php

namespace App\Providers;

use App\Events\ForgotPasswordEvent;
use App\Events\UserRegisteredEvent;
use App\Listeners\ForgotPasswordNotification;
use App\Listeners\UserRegisteredNotification;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],

        UserRegisteredEvent::class => [
            UserRegisteredNotification::class,
        ],

        ForgotPasswordEvent::class => [
          ForgotPasswordNotification::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}

<?php

namespace App\Repositories;

use App\Http\Controllers\Controller;
use App\Interfaces\AuthRepositoryInterface;
use App\Models\PasswordReset;
use App\Models\User;

class AuthRepository implements AuthRepositoryInterface {

    private User $userModel;
    private PasswordReset $passwordResetModel;

    public function __construct(
        User $userModel,
        PasswordReset $passwordResetModel
    ) {
        $this->userModel = $userModel;
        $this->passwordResetModel = $passwordResetModel;
    }

    public function verifyEmail(string $code): bool {
        return $this->userModel
            ->where(['email_verify_code' => $code, 'is_verified' => false])
            ->update(['is_verified' => true]);
    }

    public function canUpdatePassword(array $data): ?PasswordReset {
        ['email' => $email, 'password' => $password, 'token' => $token] = $data;

        return $this->passwordResetModel
            ->where(['email' => $email, 'token' => $token])
            ->first();
    }

    public function updatePassword(array $data): bool {
        ['email' => $email, 'password' => $password] = $data;

        return $this->userModel
            ->where('email', $email)
            ->update(['password' => $password]);
    }

    public function generateToken(array $data): bool {
        return $this->passwordResetModel
            ->insert($data);
    }

    public function removeToken(string $email): bool {
        return $this->passwordResetModel
            ->where('email', $email)
            ->delete();
    }

}

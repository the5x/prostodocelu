@extends('layouts.app')

@section('keywords', '')
@section('description', '')

@section('content')
    <section class="enter">
        <section class="wrapper">

            @include('includes.nav-primary')

            <div class="enter__form">
                <span class="enter__form-link">Забыли пароль</span>

                <form action="{{ route('token.send') }}" method="post">
                    @csrf

                    @error('email')<div class="error">{{ $message }}</div>@enderror

                    <div class="enter__box">
                        <input value="{{ old('email') }}" name="email" placeholder="company@company.com" class="enter__input" type="email">
                    </div>

                    <div class="enter__meta">
                        <div class="motivation__column motivation__column_mw">
                            <span class="motivation__text motivation__text_mb"><span class="motivation__text motivation__text_color">Ты причина того</span>, как ты выглядишь и всего, Что с тобой происходит. Без оправданий! Без сожалений! <span class="motivation__text motivation__text_color">ТЫ — ПРИЧИНА ВСЕГО!</span></span>
                        </div>
                        <div class="motivation__column">
                            <button class="motivation__btn pulse__btn" type="submit">Отправить</button><br/>
                        </div>
                    </div>

                </form>
            </div>


        </section>
@stop

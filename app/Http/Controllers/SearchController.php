<?php

namespace App\Http\Controllers;

use App\Models\Rate;
use App\Models\User;
use Illuminate\Http\Request;

class SearchController extends Controller {

    /**
     * Handle the incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request) {
        $searchQuery = $request->input(['s']);

        $rates = Rate::orderBy('price', 'ASC')->get();

        $users = User::query()
            ->where('first_name', 'LIKE', "%{$searchQuery}%")
            ->orWhere('last_name', 'LIKE', "%{$searchQuery}%")
            ->orWhere('email', 'LIKE', "%{$searchQuery}%")
            ->paginate(20);

        return view('profile.search', compact('rates', 'users'));
    }

}

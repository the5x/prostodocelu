<?php

namespace App\Listeners;

use App\Events\ForgotPasswordEvent;
use App\Mail\ForgotPasswordMail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Mail;

class ForgotPasswordNotification {
    public function handle(ForgotPasswordEvent $event) {
        Mail::to($event->email)->send(new ForgotPasswordMail($event->token));
    }
}

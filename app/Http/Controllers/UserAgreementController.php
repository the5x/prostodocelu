<?php

namespace App\Http\Controllers;

class UserAgreementController extends Controller {

    public function show() {
        return view('pages.user-agreement');
    }

}

<?php


namespace App\Transformers;

use Auth;

class CommentTransformer {

    public function transform(array $comment, string $profileId, string $dayId): array {
        return [
            'profile_id' => $profileId,
            'user_id' => Auth::user()->id,
            'day_id' => $dayId,
            'description' => $comment['description'],
        ];
    }

}

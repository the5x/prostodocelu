@extends('layouts.child')

@section('keywords', '')
@section('description', '')

@section('content')
    <section class="wrapper">
        <h3 class="section__title section__subtitle">Результат поиска</h3>

        @include('includes.search')

        <br><br>
    </section>
@stop

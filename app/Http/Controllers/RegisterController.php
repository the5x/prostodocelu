<?php

namespace App\Http\Controllers;

use App\Http\Requests\RegisterUserRequest;
use App\Services\UserService;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;

class RegisterController extends Controller {

    private UserService $userService;

    public function __construct(
        UserService $userService
    ) {
        $this->userService = $userService;
    }

    public function index(): View {
        return view('auth.register');
    }

    public function save(RegisterUserRequest $request): RedirectResponse {
        $this->userService->register($request->validated());
        return redirect()->route('home');
    }

}

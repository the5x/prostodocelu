<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginUserRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;

class LoginController {

    private const EMAIL_VERIFIED_TRUE = true;
    private const USER_IS_BANNED = false;

    public function index(): View {
        return view('auth.login');
    }

    private function isVerifiedEmail(): array {
        return ['is_verified' => self::EMAIL_VERIFIED_TRUE];
    }

    private function isBanned(): array {
        return ['is_banned' => self::USER_IS_BANNED];
    }

    private function receiveLoginCredentials(array $data): array {
        return array_merge($data, $this->isVerifiedEmail(), $this->isBanned());
    }

    public function login(LoginUserRequest $request): RedirectResponse {
        $credentials = $this->receiveLoginCredentials($request->only('email', 'password'));

        if (Auth::attempt($credentials)) {
            return redirect()->route('home');
        }

        return redirect()->back();
    }

    public function logout(): RedirectResponse {
        Auth::logout();
        Session::flush();

        return redirect()->route('home');
    }

}

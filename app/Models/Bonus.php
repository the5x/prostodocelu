<?php

namespace App\Models;

use App\Traits\UuidTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Bonus extends Model {

    use HasFactory, UuidTrait;

    public $timestamps = false;
    protected $table = 'bonuses';
    protected $fillable = ['title', 'video', 'description', 'created_at'];

}

<?php

namespace App\Interfaces;

use App\Models\User;

interface UserRepositoryInterface {

    public function create(array $data): User;

    public function findById(string $id): User;

    public function update(array $data, string $id): bool;

}

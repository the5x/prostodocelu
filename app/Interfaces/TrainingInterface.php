<?php


namespace App\Interfaces;


use App\Models\Day;

interface TrainingInterface {

    public function getFirstDay(): ?Day;

    public function unlockNextDay(Day $previousDay): ?Day;

}

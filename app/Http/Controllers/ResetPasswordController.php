<?php

namespace App\Http\Controllers;


use App\Http\Requests\ResetPasswordRequest;
use App\Services\AuthService;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;

class ResetPasswordController extends Controller {

    private AuthService $authService;

    public function __construct(
        AuthService $authService
    ) {
        $this->authService = $authService;
    }

    public function index($token): View {
        return view('auth.reset-password', ['token' => $token]);
    }

    public function updatePassword(ResetPasswordRequest $request): RedirectResponse {
        $meta = $request->validated();
        $updatedPassword = $this->authService->canUpdatePassword($meta);

        if (isset($updatedPassword)) {
            $this->authService->updatePassword($meta);
            $this->authService->removeToken($meta['email']);

            return redirect()->route('login.index');
        }

        return redirect()->back();
    }

}

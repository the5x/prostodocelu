@extends('layouts.child')

@section('keywords', '')
@section('description', '')

@section('content')
    <section class="days">
        <section class="wrapper">
            <div class="days__box">
                <div class="days__column days__column_mb">
                    <h1 class="section__title section__title_mt">Стартовая страница описания тренинга и программы</h1>

                    @isset($firstDay)
                        <a href="{{ route('training.show', ['user' => $user->id, 'day' => $firstDay->id]) }}"
                           class="motivation__btn pulse__btn">Перейти к тренировкам →</a>

                        <div class="bonus__video">
                            <iframe class="bonuses__iframe"
                                    src="https://www.youtube.com/embed/{{ $firstDay->video_first }}"
                                    frameborder="0"
                                    allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                                    allowfullscreen></iframe>
                        </div>

                        <div class="days__description">
                            {{ $firstDay->description }}
                        </div>

                        <div class="bonus__video">
                            <iframe class="bonuses__iframe"
                                    src="https://www.youtube.com/embed/{{ $firstDay->video_second }}"
                                    frameborder="0"
                                    allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                                    allowfullscreen></iframe>
                        </div>
                    @endisset
                </div>
            </div>
        </section>
    </section>
@stop

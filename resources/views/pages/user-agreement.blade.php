@extends('layouts.child')

@section('keywords', '')
@section('description', '')

@section('content')
    <div class="admin">
        <section class="wrapper">
            <h3 class="section__title section__subtitle">Пользовательское соглашение</h3>

            <h3 class="content__title">Regulamin korzystania ze strony ______</h3>
            <ul class="content__list">
                <li class="content__list-item">Korzystanie przez użytkownika ze strony internetowej _____ („Witryna”) oraz wszelkich produktów, kanałów, źródeł danych i usług, dostarczanych użytkownikowi na lub za pośrednictwem Witryny przez _____ (łącznie „Usługa”) podlega zapisom zawartym w Regulaminie korzystania ze strony _____ (dalej „Regulamin”). „______” oznacza _______, z którym można się skontaktować pocztą pod adresem _______ (dalej „Usługodawca”).</li>
                <li class="content__list-item">Warunki Regulaminu stanowią prawnie wiążącą umowę między tobą a Usługodawcą. Zależy nam abyś poświęcił swój czas i je uważnie przeczytał.</li>
                <li class="content__list-item">Warunki mają zastosowanie do wszystkich użytkowników Witryny. „Treść” obejmuje tekst, grafikę, zdjęcia, dźwięki, muzykę, filmy, kombinacje audiowizualne, funkcje interaktywne i inne materiały, które możesz przeglądać, uzyskiwać do nich dostęp lub umieszczać na Witrynie.</li>
            </ul>

            <h3 class="content__title">Akceptacja Regulaminu</h3>
            <ul class="content__list">
                <li class="content__list-item">Aby korzystać z Usług, musisz najpierw wyrazić zgodę na warunki zawarte w Regulaminie. Nie możesz korzystać z Usług, jeśli nie akceptujesz zawartych w nim warunków.</li>
                <li class="content__list-item">Możesz zaakceptować Warunki korzystając z Usług Usługodawcy. Rozumiesz i zgadzasz się, że Usługodawca będzie od tego momentu traktować korzystanie z Usługi jako akceptację Warunków.</li>
                <li class="content__list-item">Nie możesz korzystać z Usług i nie możesz zaakceptować Warunków, jeśli (a) nie jesteś osobą pełnoletnią, aby zawrzeć wiążącą umowę z Usługodawcą lub (b) jesteś osobą, której zakazano lub w inny sposób prawnie zabroniono otrzymywania lub korzystanie z Usługi zgodnie z prawem kraju, w którym mieszkasz lub z którego uzyskujesz dostęp lub korzystasz z Usług.</li>
            </ul>

            <h3 class="content__title">Zmiany Regulaminu</h3>
            <ul class="content__list">
                <li class="content__list-item">Usługodawca zastrzega sobie prawo do wprowadzania zmian w Warunkach, na przykład w celu uwzględnienia zmian w przepisach prawa lub zmianach regulacyjnych lub zmianach w funkcjonalności oferowanej za pośrednictwem Serwisu. Zmodyfikowana wersja Warunków („Zmodyfikowane Warunki”) zostanie opublikowana w Witrynie lub udostępniona w ramach Usługi. Jeśli nie zgadzasz się na Zmodyfikowane Warunki, musisz zaprzestać korzystania z Usługi. Dalsze korzystanie z Usług po dacie opublikowania Zmodyfikowanych Warunków będzie oznaczało akceptację Zmodyfikowanych Warunków.</li>
            </ul>

            <h3 class="content__title">Konto</h3>
            <ul class="content__list">
                <li class="content__list-item">Aby uzyskać dostęp do niektórych funkcji Witryny lub innych elementów Usługi, konieczne będzie utworzenie konta. Tworząc konto, musisz podać dokładne i kompletne informacje. Ważne jest, aby hasło do konta było bezpieczne i poufne.</li>
                <li class="content__list-item">Musisz niezwłocznie powiadomić Usługodawcę o każdym naruszeniu bezpieczeństwa lub nieautoryzowanym użyciu Twojego konta.</li>
                <li class="content__list-item">Zgadzasz się, że będziesz ponosił wyłączną odpowiedzialność (przed Usługodawcą i innymi) za wszelką aktywność, która ma miejsce w ramach Twojego konta.</li>
                <li class="content__list-item">Usługodawca może od czasu do czasu kontaktować się z Tobą w sprawie usług i produktów stron trzecich, jeśli wyraziłeś na to wyraźną lub wyraziłeś dorozumianą zgodę. </li>
            </ul>

            <h3 class="content__title">Ogólne ograniczenia użytkowania</h3>
            <ul class="content__list">
                <li class="content__list-item">Usługodawca niniejszym udziela Ci pozwolenia na dostęp i korzystanie z Usług, z zastrzeżeniem następujących wyraźnych warunków, a ty zgadzasz się, że nieprzestrzeganie któregokolwiek z tych warunków będzie stanowić naruszenie niniejszych Warunków z twojej winy:</li>
                <li class="content__list-item">zgadzasz się nie rozpowszechniać żadnej części Witryny lub Usług, w tym między innymi Treści, na jakimkolwiek nośniku bez uprzedniej pisemnej zgody Usługodawcy, chyba że udostępni on środki do takiej dystrybucji poprzez dedykowaną do tego funkcjonalność;</li>
                <li class="content__list-item">zgadzasz się nie zmieniać ani nie modyfikować żadnej części Witryny ani Usługi;</li>
                <li class="content__list-item">Użytkownik zgadza się nie (ani nie próbować) obchodzić, wyłączać lub w inny sposób ingerować w funkcje związane z bezpieczeństwem Usługi lub jej funkcjami;</li>
                <li class="content__list-item">
                    zgadzasz się nie korzystać z Usług w żadnym z następujących zastosowań komercyjnych, chyba że uzyskasz uprzednią pisemną zgodę Usługodawcy:
                    <ul class="content__list">
                        <li class="content__list-item">sprzedaż lub udzielenie dostępu do Witryny;</li>
                        <li class="content__list-item">sprzedaż reklam, sponsoringu lub promocji umieszczonych na Witrynie;</li>
                    </ul>
                </li>
                <li class="content__list-item">zgadzasz się nie przechowywać ani nie zbierać żadnych danych osobowych użytkowników Witryny lub jakiejkolwiek Usługi;</li>
                <li class="content__list-item">zgadzasz się nie nakłaniać innych Użytkowników Witryny do działań komercyjnych lub zarobkowych;</li>
                <li class="content__list-item">Nie wolno kopiować, powielać, rozpowszechniać, transmitować, wyświetlać, sprzedawać, licencjonować ani w inny sposób wykorzystywać treści Witryny bez uprzedniej pisemnej zgody Usługodawcy lub odpowiednich licencjodawców treści.</li>
                <li class="content__list-item">Przyjmujesz do wiadomości i zgadzasz się, że Usługodawca może zaprzestać (na stałe lub czasowo) świadczenia Usług (lub jakichkolwiek funkcji w ramach Usług) bez wcześniejszego powiadomienia.</li>
                <li class="content__list-item">Zgadzasz się, że ponosisz wyłączną odpowiedzialność (i że Usugodawca nie ponosi żadnej odpowiedzialności wobec Ciebie ani osób trzecich) za twoje działania własne realizowane w ramach świadczonych Usług, interpretację i wykorzystanie Usług, a tym samym zwalniasz Usługodawcę z wszelkiej odpowiedzialności wynikającej z twoich działań.</li>
            </ul>


            <h3 class="content__title">Prawa autorskie</h3>
            <ul class="content__list">
                <li class="content__list-item">Usługodawca szanuje prawa autorskie osób trzecich. Usunie wszelkie treści, które zostaną zidentyfikowane jako należące do strony trzeciej – możesz to zrobić, kontaktując się z nami za pośrednictwem poczty elektronicznej.</li>
                <li class="content__list-item">Zgodnie z polityką Regulaminem, Usługodawca zablokuje dostęp użytkownika do Usług, jeśli użytkownik naruszy prawa i obowiązki wynikające z Regulaminu.</li>
            </ul>


            <h3 class="content__title">Treść</h3>
            <ul class="content__list">
                <li class="content__list-item">Jako posiadacz konta możesz przesyłać Treści. Rozumiesz, że bez względu na to, czy Treść zostanie opublikowana, Usługodawca nie gwarantuje poufności w odniesieniu do przedmiotowej Treści.</li>
                <li class="content__list-item">Zachowujesz wszystkie prawa własności do umieszczonych przez siebie Treści, ale przyznajesz ograniczone prawa licencyjne do tych nich Usługodawcy.</li>
                <li class="content__list-item">Rozumiesz i zgadzasz się, że ponosisz wyłączną odpowiedzialność za swoje Treści jak i ponosisz konsekwencje ich opublikowania.</li>
                <li class="content__list-item">Oświadczasz i gwarantujesz, że posiadasz wszystkie niezbędne licencje, prawa, zgody i zezwolenia, które są wymagane, aby umożliwić Usługodawcy korzystanie z twoich Treści w celu świadczenia Usług.</li>
                <li class="content__list-item">Zgadzasz się, że nie będziesz publikować ani przesyłać żadnych Treści zawierających materiały, których posiadanie jest niezgodne z prawem w kraju, w którym mieszkasz, lub których używanie lub posiadanie przez Usługodawcę byłoby niezgodne z prawem w związku ze świadczeniem Usługi.</li>
                <li class="content__list-item">Po uzyskaniu wiedzy o potencjalnym naruszeniu warunków Regulaminu, Usługodawca zastrzega sobie prawo (ale nie ma obowiązku) do decydowania, czy Treść jest zgodna z wymaganiami dotyczącymi treści określonymi w niniejszy Regulaminie i może usunąć taką treść i/lub ograniczyć dostęp Użytkownika dla przesyłania określonych treści, bez wcześniejszego powiadomienia i według własnego uznania.</li>
            </ul>


            <h3 class="content__title">Licencje</h3>
            <ul class="content__list">
                <li class="content__list-item">Gdy przesyłasz lub publikujesz Treści na Witrynie, udzielasz Usługodawcy ogólnoświatową, niewyłączną, nieodpłatną, zbywalną licencję (z prawem do udzielania sublicencji) na używanie, powielanie, rozpowszechnianie, przygotowywanie prac pochodnych, wyświetlanie i wykonywanie tych Treści Usługodawcy, w tym między innymi do promowania i redystrybucji części lub całości Usług (oraz prac pochodnych) w dowolnych formatach medialnych i za pośrednictwem dowolnych kanałów medialnych;</li>
                <li class="content__list-item">Powyższa licencja wygasa, gdy usuniesz swoją Treść z Witryny. Powyższe licencja jest bezterminowa i nieodwołalna.</li>
            </ul>


            <h3 class="content__title">Zawartość Witryny</h3>
            <ul class="content__list">
                <li class="content__list-item">Z wyjątkiem Treści przesłanych przez użytkownika, wszelkie inne Treści umieszczone na Witrynie są własnością lub są licencjonowane przez Usługodawcę i podlegają prawom autorskim, prawom do znaków towarowych oraz innym prawom własności intelektualnej Usługodawcy lub licencjodawców Usługodawcy. Wszelkie znaki towarowe lub usługowe osób trzecich obecne na Witrynie, które nie zostały przesłane lub opublikowane przez użytkownika, są znakami towarowymi lub usługowymi ich odpowiednich właścicieli. Takie treści nie mogą być pobierane, kopiowane, reprodukowane, rozpowszechniane, transmitowane, wyświetlane, sprzedawane, licencjonowane lub w inny sposób wykorzystywane do jakichkolwiek innych celów bez uprzedniej pisemnej zgody Usługodawcy.</li>
            </ul>

            <h3 class="content__title">Hiperłącza</h3>
            <ul class="content__list">
                <li class="content__list-item">Usługa może zawierać hiperłącza do innych witryn internetowych, które nie są własnością ani nie są kontrolowane przez _____. _____ nie ma kontroli nad treścią, polityką prywatności ani praktykami stron internetowych osób trzecich i nie ponosi za nią odpowiedzialności.</li>
                <li class="content__list-item">Usługodawca zachęca do zapoznania się z regulaminem i polityką prywatności każdej innej odwiedzanej witryny.</li>
            </ul>

            <h3 class="content__title">Zakończenie świadczenia Usług</h3>
            <ul class="content__list">
                <li class="content__list-item">Warunki Regulaminu będą obowiązywać do czasu ich rozwiązania przez Ciebie lub Usługodawcę.</li>
                <li class="content__list-item">Jeśli chcesz rozwiązać umowę z Usługodawcą, możesz to zrobić w dowolnym momencie poprzez zamknięcie swojego konta.</li>
                <li class="content__list-item">
                    Usługodawca może w dowolnym momencie rozwiązać umowę z tobą, jeśli:
                    <ul class="content__list">
                        <li class="content__list-item">naruszyłeś jakiekolwiek postanowienie Regulaminu (lub działałeś w sposób, który wyraźnie wskazuje, że nie zamierzasz lub nie jesteś w stanie przestrzegać postanowień Regulaminu);</li>
                        <li class="content__list-item">Usługodawca jest do tego zobowiązany przez prawo (na przykład, gdy świadczenie Usługi jest lub stanie się niezgodne z prawem);</li>
                        <li class="content__list-item">Usługodawca zaprzestaje świadczenia Usługi użytkownikom w kraju, w którym mieszkasz.</li>
                    </ul>
                </li>
            </ul>

            <h3 class="content__title">Gwarancja</h3>
            <ul class="content__list">
                <li class="content__list-item">Usługodawca nie udziela żadnych gwarancji w ramach świadczonych Usług.</li>
                <li class="content__list-item">W szczególności Usługodawca nie oświadcza ani nie gwarantuje, że:
                    <ul class="content__list">
                        <li class="content__list-item">korzystanie przez ciebie z Usługi spełni Twoje oczekiwania</li>
                        <li class="content__list-item">korzystanie przez Ciebie z Usługi będzie nieprzerwane, terminowe, bezpieczne lub wolne od błędów.</li>
                    </ul>
                </li>
            </ul>

            <h3 class="content__title">RODO</h3>
            <ul class="content__list">
                <li class="content__list-item">Regulamin jaki i działania realizowane przez Usługodawcę są zgodne z warunkami Rozporządzenia Parlamentu Europejskiego i Rady (UE) 2016/679 z dnia 27 kwietnia 2016 r. w sprawie ochrony osób fizycznych w związku z przetwarzaniem danych osobowych i w sprawie swobodnego przepływu takich danych oraz uchylenia dyrektywy 95/46/WE</li>
            </ul>

        </section>
    </div>
@stop

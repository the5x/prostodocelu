<div class="header__box header__box_pb">
    <a class="header__logo" href="{{ route('home')  }}">
        <picture>
            <source srcset="{{ URL::to('/assets/images/logo-small.png')  }}" media="(max-width: 880px)"/>
            <img class="header__logo-pic" src="{{ URL::to('/assets/images/logo-big.png')  }}" alt="">
        </picture>
    </a>
    <nav class="header__nav">
        <ul class="header__list">
            @auth
                @if(auth()->user()->is_admin)
                    <li class="header__list-item">
                        <a class="header__list-item-link" href="{{ route('profile.all') }}">Пользователи</a>
                    </li>
                    <li class="header__list-item">
                        <a class="header__list-item-link" href="{{ route('comment.all') }}">Комментарии</a>
                    </li>
                    <li class="header__list-item">
                        <a class="header__list-item-link" href="{{ route('day.all') }}">Дни</a>
                    </li>
                @endif
                <li class="header__list-item">
                    <a class="header__list-item-link" href="{{ route('rate.index') }}">Тарифы</a>
                </li>
                <li class="header__list-item">
                    <a class="header__list-item-link"
                       href="{{ route('training.start', ['user' => auth()->user()->id]) }}">Программа</a>
                </li>
                <li class="header__list-item">
                    <a class="header__list-item-link" href="{{ route('bonus.all') }}">Бонусы</a>
                </li>
                <li class="header__list-item">
                    <a class="header__list-item-link" href="{{ route('profile.show', ['user' => Auth::user()->id]) }}">Профиль</a>
                </li>
                <li class="header__list-item">
                    <form action="{{ route('logout') }}" method="post">
                        @csrf
                        <button class="header__list-item-link">
                            <img src="{{ URL::to('/assets/images/ico-offline.png') }}" alt="">
                        </button>
                    </form>
                </li>
            @endauth

            @guest
                <li class="header__list-item">
                    <a class="header__list-item-link header__list-item-link_lock" href="{{ route('login.index') }}">
                        Войти
                    </a>
                </li>
            @endguest
        </ul>
    </nav>
</div>

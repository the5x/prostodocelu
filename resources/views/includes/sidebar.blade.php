<div class="days__profile">
    <div class="days__profile-column days__profile-column_mw">

        @if(empty($user->photo))
            <img class="days__cover"
                 src="{{ URL::to('/assets/images/no-photo.svg') }}"
                 alt="">
        @else
            <img class="days__cover"
                 src="{{ URL::to('/uploads/' . $user->photo) }}"
                 alt="">
        @endif

    </div>
    <div class="days__profile-column">
        <a class="days__name-link" href="{{ route('profile.show', ['user' => $user->id ]) }}">
            {{ $user->first_name ?: 'Name'  }} {{ $user->last_name ?: 'Surname' }}
        </a>
        <small class="days__subtitle">
            → Тарифный план:
            {{ $premiumAccount ?: 'Бесплатный' }}
        </small>
    </div>
</div>

<ul class="days__list">
    @foreach($days as $day)
        @if($loop->first || ($loop->iteration <= $lastFreeDay && $user->days->contains($day)))
            <li class="days__list-item">
                <a class="days__list-item-link {{ request()->is('users/'.$user->id.'/days/'.$day->id ) ? 'days__list-item-link_bold' : '' }}"
                   href="/users/{{ $user->id }}/days/{{ $day->id }}">
                    {{ $day->title }}
                </a>
            </li>
        @elseif($loop->iteration >= $lastFreeDay && $premiumAccount && $user->days->contains($day))
            <li class="days__list-item">
                <a class="days__list-item-link {{ request()->is('users/'.$user->id.'/days/'.$day->id ) ? 'days__list-item-link_bold' : '' }}"
                   href="/users/{{ $user->id }}/days/{{ $day->id }}">
                    {{ $day->title }}
                </a>
            </li>
        @else
            <li class="days__list-item">
                <span class="days__list-item-link days__list-item-link_grey">{{ $day->title }}</span>
            </li>
        @endif



    @endforeach

</ul>

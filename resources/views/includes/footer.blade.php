<footer class="footer">
    <div class="wrapper">
        <nav class="footer__nav">
            <ul class="footer__list">
                @include('includes.nav-footer')
            </ul>
        </nav>

        <h3 class="page__title">prosto do celu</h3>
        <h4 class="page__subtitle">Prosto do Celu — Schudnij w 100 dni</h4>
    </div>
</footer>

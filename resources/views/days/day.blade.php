@extends('layouts.child')

@section('keywords', '')
@section('description', '')

@section('content')
    <section class="days">
        <section class="wrapper">
            <div class="days__box">
                <div class="days__column days__column_mb">
                    <a href="{{ route('day.edit', ['day' => $day->id]) }}">
                        <img class="profile__ico-edit"
                             src="{{ URL::to('/assets/images/ico-edit.svg') }}"
                             alt="">
                    </a>

                    <h1 class="section__title">{{ $day->title }}</h1>

                    <div class="bonus__video">
                        <iframe class="bonuses__iframe" src="https://www.youtube.com/embed/{{ $day->video_first }}"
                                frameborder="0"
                                allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                                allowfullscreen></iframe>
                    </div>

                    <div class="days__description">
                        {{ $day->description }}
                    </div>

                    <div class="bonus__video">
                        <iframe class="bonuses__iframe" src="https://www.youtube.com/embed/{{ $day->video_second }}"
                                frameborder="0"
                                allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                                allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </section>
    </section>
@stop

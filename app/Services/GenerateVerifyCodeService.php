<?php

namespace App\Services;
use Illuminate\Support\Str;

class GenerateVerifyCodeService {
    private const RANDOM_NUMBER = 40;

    public function generateCode(): string {
        return Str::random(self::RANDOM_NUMBER);
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\Rate;
use App\Models\User;
use App\Services\DayService;
use App\Services\UserService;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;

class TrainingController extends Controller {

    private const LAST_FREE_DAY = 14;
    private UserService $userService;
    private DayService $dayService;

    public function __construct(
        UserService $userService,
        DayService $dayService
    ) {
        $this->userService = $userService;
        $this->dayService = $dayService;
    }

    private function hasUserPremiumRate(User $user) {
        if (!is_null($user->rate_id) && $user->rate->price > 0) {
            return $user->rate->title;
        }

        return null;
    }

    public function startTraining(string $userId): View {
        $user = $this->userService->findById($userId);
        $firstDay = $this->dayService->getFirstDay();

        return view('days.start', compact('user', 'firstDay'));
    }

    public function showTraining(string $userId, string $dayId): View {
        $user = $this->userService->findById($userId);
        $premiumAccount = $this->hasUserPremiumRate($user);
        $currentDay = $this->dayService->findDayForCurrentUser($userId, $dayId);
        $days = $this->dayService->all();

        return view('days.training', [
            'day' => $currentDay,
            'user' => $user,
            'days' => $days,
            'premiumAccount' => $premiumAccount,
            'lastFreeDay' => self::LAST_FREE_DAY,
        ]);
    }

    public function openTraining(string $userId, string $dayId): RedirectResponse {
        $user = $this->userService->findById($userId);
        $currentDay = $this->dayService->findDayForCurrentUser($userId, $dayId);
        $nextDay = $this->dayService->unlockNextDay($currentDay);

        $user->days()->attach($nextDay);

        return redirect()->back();
    }

}

<?php


namespace App\Repositories;


use App\Interfaces\RateRepositoryInterface;
use App\Models\Rate;
use App\Models\User;
use Illuminate\Database\Eloquent\Collection;

class RateRepository implements RateRepositoryInterface {

    private User $userModel;
    private Rate $rateModel;

    public function __construct(
        User $userModel,
        Rate $rateModel
    ) {
        $this->userModel = $userModel;
        $this->rateModel = $rateModel;
    }

    public function create(array $data): Rate {
        return $this->rateModel
            ->create($data);
    }

    public function findById(string $id): Rate {
        return $this->rateModel
            ->findOrFail($id);
    }

    public function update(string $id, array $data): bool {
        return $this->rateModel
            ->where('id', $id)
            ->update($data);
    }

    public function all(): Collection {
        return $this->rateModel
            ->orderBy('price')
            ->get();
    }

    public function delete(string $id): bool {
        return $this->rateModel
            ->where('id', $id)
            ->delete();
    }

    public function chooseAnotherRate(string $userId, string $rateId): bool {
        return $this->userModel
            ->where('id', $userId)
            ->update([
                'rate_id' => $rateId,
            ]);
    }

}

<?php


namespace App\Transformers;


use App\Services\HashPasswordService;

class PasswordTransformer {

    private HashPasswordService $hashService;

    public function __construct(
        HashPasswordService $hashService
    ) {
        $this->hashService = $hashService;
    }

    public function transform(array $data): array {
        ['email' => $email, 'password' => $password] = $data;

        return [
            'email' => $email,
            'password' => $this->hashService->hashPassword($password)
        ];
    }

}

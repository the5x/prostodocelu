<?php


namespace App\Interfaces;


use App\Models\Bonus;
use Illuminate\Database\Eloquent\Collection;

interface BonusRepositoryInterface {

    public function create(array $data): Bonus;

    public function show(string $id): Bonus;

    public function update(array $data, string $id): bool;

    public function all(): Collection;

    public function delete(string $id): bool;

}

<?php


namespace App\Interfaces;


use App\Models\Diary;

interface DiaryRepositoryInterface {

    public function create(array $data): Diary;

    public function findById(string $id): Diary;

    public function update(array $data, string $id): bool;

    public function delete(string $id): bool;

}

<?php

namespace App\Http\Controllers;

use App\Services\AuthService;
use Illuminate\Http\RedirectResponse;

class VerificationEmailController extends Controller {

    private AuthService $authService;

    public function __construct(
        AuthService $authService
    ) {
        $this->authService = $authService;
    }

    public function verifyEmail($code): RedirectResponse {
        $userVerified = $this->authService->verifyEmail($code);

        if (isset($userVerified)) {
            return redirect()->route('login.index');
        }

        return redirect()->route('home');
    }

}

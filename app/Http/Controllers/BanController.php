<?php


namespace App\Http\Controllers;


use App\Services\BanService;
use Illuminate\Http\RedirectResponse;

class BanController extends Controller {

    private BanService $banService;

    public function __construct(
        BanService $banService
    ) {
        $this->banService = $banService;
    }

    public function ban(string $id): RedirectResponse {
        $this->banService->ban($id);
        return redirect()->route('profile.show', ['user' => $id]);
    }

    public function unban(string $id): RedirectResponse {
        $this->banService->unban($id);
        return redirect()->route('profile.show', ['user' => $id]);
    }

}

<?php


namespace App\Interfaces;


use App\Models\Rate;
use Illuminate\Database\Eloquent\Collection;

interface RateRepositoryInterface {

    public function create(array $data): Rate;

    public function findById(string $id): Rate;

    public function update(string $id, array $data): bool;

    public function all(): Collection;

    public function delete(string $id): bool;

    public function chooseAnotherRate(string $userId, string $rateId): bool;

}

<?php


namespace App\Transformers;


use Carbon\Carbon;

class DiaryTransformer {

    public function transform(array $data): array {
        return [
            'description' => $data['description'],
            'user_id' => auth()->user()->id,
            'created_at' => Carbon::now(),
        ];
    }

}

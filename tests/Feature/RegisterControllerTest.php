<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class RegisterControllerTest extends TestCase {

    use RefreshDatabase;

    public function test_show_register_page(): void {
        $this->get(route('register.index'))
            ->assertOk()
            ->assertViewIs('auth.register');

        $this->assertGuest();
    }

    public function test_make_redirect_if_authorized_user_tries_show_register_page(): void {
        $user = User::factory()->create();
        $this->actingAs($user);

        $this->get(route('register.index'))
            ->assertStatus(302)
            ->assertRedirect(route('home'));
    }

    public function test_register_user_with_unique_data(): void {
        $registerUserData = ['email' => 'randomEmail@gmail.com', 'password' => 'randomPasswordString'];

        $this->post(route('register.save'), $registerUserData);

        $this->assertDatabaseHas('users', ['email' => $registerUserData['email']]);
    }

    public function test_register_user_with_duplicate_data(): void {
        User::factory(['email' => 'randomEmail@gmail.com'])->create();
        $registerUserData = ['email' => 'randomEmail@gmail.com', 'password' => 'randomPasswordString'];

        $this->post(route('register.save'), $registerUserData)
            ->assertSessionHasErrors([
                'email' => 'The email has already been taken.'
            ]);

        $this->assertDatabaseHas('users', ['email' => $registerUserData['email']]);
    }

}

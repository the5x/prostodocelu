<?php


namespace App\Transformers;


use App\Services\GenerateVerifyCodeService;

class TokenTransformer {

    private GenerateVerifyCodeService $verifyService;

    public function __construct(
        GenerateVerifyCodeService $verifyService
    ) {
        $this->verifyService = $verifyService;
    }

    public function transform($data) {
        $email = $data['email'];
        $token = $this->verifyService->generateCode();

        return ['email' => $email, 'token' => $token];
    }

}

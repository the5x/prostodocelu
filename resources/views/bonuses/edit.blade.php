@extends('layouts.child')

@section('keywords', '')
@section('description', '')

@section('content')
    <div class="admin">
        <section class="wrapper">
            <h3 class="section__title section__subtitle">Создать бонус</h3>

            <form action="{{ route('bonus.update', ['bonus' => $bonus->id ]) }}" method="POST">
                @method('PUT')
                @csrf

                <div class="form__table">
                    <label class="form__label" for="title">Название</label>
                    @error('title')
                    <div class="error">{{ $message }}</div>@enderror
                    <input type="text" class="form__input" name="title" value="{{ old('title') ?: $bonus->title }}"/>

                    <label class="form__label" for="video">Видео</label>
                    <small class="form__small">Вставлять только код, например: youtube.com/watch?v=<small
                            class="form__small form__small_yellow">i0GuCEKUDRU</small></small>
                    @error('video')
                    <div class="error">{{ $message }}</div>@enderror
                    <input type="text" class="form__input" name="video" value="{{ old('video') ?: $bonus->video }}"/>

                    <label class="form__label" for="description">Описание</label>
                    @error('description')
                    <div class="error">{{ $message }}</div>@enderror
                    <textarea class="form__input form__input_textarea form__input_border" name="description"
                              cols="30" rows="10">{{ old('description') ?: $bonus->description }}</textarea>
                </div>
                <br><br>
                <button class="motivation__btn pulse__btn" type="submit">Обновить бонус</button>
            </form>
            <form action="{{ route('bonus.delete', ['bonus' => $bonus->id ]) }}" method="POST">
                @method('DELETE')
                @csrf
                <button class="edit__button" type="submit">Удалить</button>
            </form>
        </section>
    </div>
@stop

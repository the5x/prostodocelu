<?php

namespace App\Models;

use App\Traits\UuidTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Day extends Model {

    use HasFactory, UuidTrait;

    public $timestamps = false;
    protected $table = 'days';
    protected $fillable = ['title', 'video_first', 'video_second', 'description'];

    public function comments(): HasMany {
        return $this->hasMany(Comment::class)->orderBy('created_at', 'ASC');
    }

}

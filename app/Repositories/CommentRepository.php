<?php


namespace App\Repositories;


use App\Interfaces\CommentInterface;
use App\Models\Comment;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

class CommentRepository implements CommentInterface {

    private const PAGINATE = 20;
    private Comment $commentModel;

    public function __construct(
        Comment $commentModel
    ) {
        $this->commentModel = $commentModel;
    }

    public function save(array $data): Comment {
        return $this->commentModel
            ->create($data);
    }

    public function all(): LengthAwarePaginator {
        return $this->commentModel::with('day', 'user')
            ->orderBy('created_at', 'DESC')
            ->paginate(self::PAGINATE);
    }

    public function delete(string $id): bool {
        return $this->commentModel
            ->where('id', $id)
            ->delete();
    }

}

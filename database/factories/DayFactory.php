<?php

namespace Database\Factories;

use App\Models\Day;
use Illuminate\Database\Eloquent\Factories\Factory;

class DayFactory extends Factory {

    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Day::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition() {
        return [
            'title' => $this->faker->text($maxNbChars = 100),
            'video_first' => $this->faker->randomElement(['8DZktowZo_k', 'VdxDnuXj4yc', 'TKq11MghPqk', '9MjJRAtg-OM', 'TFO9hBtLVec', '5if4cjO5nxo']),
            'video_second' => $this->faker->randomElement(['8DZktowZo_k', 'VdxDnuXj4yc', 'TKq11MghPqk', '9MjJRAtg-OM', 'TFO9hBtLVec', '5if4cjO5nxo']),
            'description' => $this->faker->realText($maxNbChars = 200),
            'created_at' => $this->faker->dateTimeBetween($startDate = '-30 years', $endDate = 'now'),
        ];
    }

}

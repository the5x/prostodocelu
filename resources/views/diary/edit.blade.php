@extends('layouts.child')

@section('keywords', '')
@section('description', '')

@section('content')
    <div class="admin">
        <section class="wrapper">
            <h3 class="section__title section__subtitle">Редактировать запись</h3>

            <form action="{{ route('diary.update', ['diary' => $diary->id]) }}" method="POST">
                @method('PUT')
                @csrf

                <div class="form__table">
                    <label class="form__label" for="description">Дневник</label>
                    @error('description')
                    <div class="error">{{ $message }}</div>@enderror
                    <textarea class="form__input form__input_textarea form__input_border" name="description"
                              cols="30" rows="10">{{ old('description') ?: $diary->description  }}</textarea>
                </div>
                <br><br>
                <button class="motivation__btn pulse__btn" type="submit">Обновить запись</button>
            </form>


            <form action="{{ route('diary.delete', ['diary' => $diary->id])  }}" method="POST">
                @method('DELETE')
                @csrf
                <button class="edit__button">Удалить</button>
            </form>
        </section>
    </div>
@stop
;

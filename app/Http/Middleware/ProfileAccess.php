<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class ProfileAccess {

    public function handle(Request $request, Closure $next) {
        if (auth()->user()->is_admin || (auth()->check() && auth()->user()->id === $request->route('user'))) {
            return $next($request);
        }

        abort(404);
    }

}


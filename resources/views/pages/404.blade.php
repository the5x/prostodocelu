@extends('layouts.child')

@section('keywords', '')
@section('description', '')

@section('content')
    <section class="found">
        <section class="wrapper">
            <h1 class="section__title">404</h1>
            <p class="found__text">По вашему запросу ничего не найдено</p>
            <div class="found__box">
                <div class="found__column found__column_right">
                    <img src="{{ URL::to('/assets/images/404-01.png') }}" alt="">
                </div>
                <div class="found__column found__column_center">
                    <img src="{{ URL::to('/assets/images/404-02.png') }}" alt="">
                </div>
                <div class="found__column found__column_right">
                    <img src="{{ URL::to('/assets/images/404-03.png') }}" alt="">
                </div>
            </div>
        </section>
    </section>
@stop

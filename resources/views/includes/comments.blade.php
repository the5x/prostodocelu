<div class="comment">
    <div class="comment__profile">
        <div class="comment__profile_column">
            @if(empty($comment->user->photo))
                <img class="comment__profile-pic"
                     src="{{ URL::to('/assets/images/no-photo.svg') }}"
                     alt="">
            @else
                <img class="comment__profile-pic"
                     src="{{ URL::to('/uploads/' . $comment->user->photo) }}"
                     alt="">
            @endif
        </div>

        <div class="comment__profile_column">
            @if(auth()->user()->is_admin)
                <a href="{{ route('profile.show', ['user' => $comment->user->id]) }}"
                   class="comment__profile_title">
                    {{ $comment->user->first_name ?? 'Name' }} {{ $comment->user->last_name ?? 'Surname' }}
                </a>
            @else
                <span class="comment__profile_title">
                    {{ $comment->user->first_name ?? 'Name' }} {{ $comment->user->last_name ?? 'Surname' }}
                </span>
            @endif

            @if($comment->user->is_admin)
                <img class="comment__status-ico"
                     src="{{ URL::to('/assets/images/ico-status.svg') }}"
                     alt="">
            @endif

            <time class="comment__profile_time">{{ $comment->created_at  }}</time>
            <p class="comment__description">{!! $comment->description !!}</p>

            <br/>

            @if(auth()->user()->id === $comment->user->id || auth()->user()->is_admin)
                <form action="{{ route('comment.delete', ['comment' => $comment->id]) }}"
                      method="POST">
                    @method('DELETE')
                    @csrf
                    <button type="submit" class="edit__button">Удалить</button>
                </form>
            @endif
        </div>
    </div>
</div>

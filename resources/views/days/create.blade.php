@extends('layouts.child')

@section('keywords', '')
@section('description', '')

@section('content')
    <div class="admin">
        <section class="wrapper">
            <h3 class="section__title section__subtitle">Создать тренировачный день</h3>

            <form action="{{ route('day.save') }}" method="POST">
                @csrf

                <div class="form__table">
                    <label class="form__label" for="title">Название</label>
                    @error('title')
                    <div class="error">{{ $message }}</div>@enderror
                    <input type="text" class="form__input" name="title" value="{{ old('title') }}"/>

                    <label class="form__label" for="video_first">Первое видео</label>
                    <small class="form__small">Вставлять только код, например: youtube.com/watch?v=<small
                            class="form__small form__small_yellow">i0GuCEKUDRU</small></small>
                    @error('video_first')
                    <div class="error">{{ $message }}</div>@enderror
                    <input type="text" class="form__input" name="video_first" value="{{ old('video_first') }}"/>

                    <label class="form__label" for="video_second">Второе видео</label>
                    <small class="form__small">Вставлять только код, например: youtube.com/watch?v=<small
                            class="form__small form__small_yellow">i0GuCEKUDRU</small></small>
                    @error('video_second')
                    <div class="error">{{ $message }}</div>@enderror
                    <input type="text" class="form__input" name="video_second" value="{{ old('video_second') }}"/>

                    <label class="form__label" for="description">Описание</label>
                    @error('description')
                    <div class="error">{{ $message }}</div>@enderror
                    <textarea class="form__input form__input_textarea form__input_border" name="description"
                              cols="30" rows="10">{{ old('description') }}</textarea>
                </div>
                <br><br>
                <button class="motivation__btn pulse__btn" type="submit">Создать тренировачный день</button>
            </form>

        </section>
    </div>
@stop

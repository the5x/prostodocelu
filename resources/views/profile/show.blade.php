@extends('layouts.child')

@section('keywords', '')
@section('description', '')

@section('content')
    <section class="profile">
        <div class="wrapper">
            @if($user->is_banned)
                <span class="profile__premium">
                    banned
                </span>
            @endif
            <h1 class="section__title">ДАННЫЕ ПРОФИЛЯ</h1>
            <div class="profile__box">
                <div class="profile__column profile__column_mr">
                    <a href="{{ route('profile.edit', ['user' => $user->id])  }}">
                        <img class="profile__ico-edit" src="{{ URL::to('/assets/images/ico-edit.svg') }}" alt="">
                    </a>
                    <h3 class="profile__title">
                        {{ $user->first_name ?: 'Name'  }}<br/>{{ $user->last_name ?: 'Surname' }}
                    </h3>

                    <div class="profile__cover">
                        @if(empty($user->photo))
                            <img class="profile__cover-pic" src="{{ URL::to('/assets/images/no-photo.svg') }}"
                                 alt="">
                        @else
                            <img class="profile__cover-pic" src="{{ URL::to('/uploads/' . $user->photo) }}"
                                 alt="">
                        @endif
                        <div class="profile__age"
                             style="background-image: url({{ URL::to('/assets/images/ico-age.svg') }})">
                            {{ $user->age  }} y.o
                        </div>
                    </div>
                    <div class="profile__meta">
                        <div class="profile__meta-column">
                            @if($user->sex === 'M')
                                <img class="profile__ico" src="{{ URL::to('/assets/images/ico-man.svg')  }}" alt="">
                                <strong class="profile__abbreviation">M</strong>
                            @elseif($user->sex === 'W')
                                <img class="profile__ico" src="{{ URL::to('/assets/images/ico-woman.svg')  }}" alt="">
                                <strong class="profile__abbreviation">W</strong>
                            @else
                                <img class="profile__ico" src="{{ URL::to('/assets/images/ico-woman.svg')  }}" alt="">
                                <strong class="profile__abbreviation">NA</strong>
                            @endif
                        </div>
                        <div class="profile__meta-column">
                            <img class="profile__ico" src="{{ URL::to('/assets/images/ico-height.svg')  }}" alt="">
                            <strong class="profile__abbreviation">{{ $user->height }}</strong>
                        </div>
                        <div class="profile__meta-column">
                            <img class="profile__ico" src="{{ URL::to('/assets/images/ico-weight.svg')  }}" alt="">
                            <strong class="profile__abbreviation">{{ $user->weight }}</strong>
                        </div>
                    </div>
                    <div class="profile__purpose">
                        <strong class="profile__purpose-title">Цель</strong>
                        <p class="profile__purpose-text">{{ $user->purpose }}</p>
                    </div>

                </div>
                <div class="profile__column">

                    @if(auth()->user()->id === $user->id)
                        <a href="{{ route('diary.create') }}">
                            <img class="profile__ico-edit"
                                 src="{{ URL::to('/assets/images/ico-edit.svg') }}"
                                 alt="">
                        </a>
                    @endif
                    <h3 class="profile__title">дневник</h3>

                    <table class="profile__table">
                        <tr class="profile__tr">
                            <th class="profile__th">Дата</th>
                            <th class="profile__th">Описание</th>
                        </tr>

                        @foreach($user->diaries as $diary)
                            <tr class="profile__tr">
                                <td class="profile__td">
                                    <a class="profile__td-link"
                                       href="{{ route('diary.edit', ['diary' => $diary->id]) }}">{{ $diary->created_at }}</a>
                                </td>
                                <td class="profile__td">
                                    {{ $diary->description }}
                                </td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </section>
@stop

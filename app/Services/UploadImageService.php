<?php

namespace App\Services;

use Illuminate\Http\UploadedFile;

class UploadImageService {

    private const UPLOAD_IMAGE_DIRECTORY = 'uploads';

    public function generatePhotoName(UploadedFile $file): string {
        return time() . '.' . $file->extension();
    }

    public function savePhotoToDirectory(UploadedFile $file): string {
        $image = $this->generatePhotoName($file);
        $uploadedImage = $file->move(public_path(self::UPLOAD_IMAGE_DIRECTORY), $image);

        return $uploadedImage->getFilename();
    }

}

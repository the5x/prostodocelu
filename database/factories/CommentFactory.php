<?php

namespace Database\Factories;

use App\Models\Comment;
use App\Models\Day;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class CommentFactory extends Factory {

    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Comment::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition() {
        return [
            'description' => 'Направленный маркетинг отражает креатив',
            'user_id' => User::factory()->create(),
            'day_id' => Day::factory()->create(),
        ];
    }

}

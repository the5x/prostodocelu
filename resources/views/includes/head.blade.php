<meta charset="UTF-8">
<meta name="viewport"
      content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<meta name="keywords" content="@yield('keywords')" />
<meta name="description" content="@yield('description')" />
<link rel="stylesheet" href="{{ URL::to('assets/styles/css.css')  }}">
<title>Prosto do celu</title>

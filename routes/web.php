<?php

use App\Http\Controllers\BanController;
use App\Http\Controllers\BonusController;
use App\Http\Controllers\CKEditorController;
use App\Http\Controllers\CommentController;
use App\Http\Controllers\DayController;
use App\Http\Controllers\DiaryController;
use App\Http\Controllers\ForgotPasswordController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\RateController;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\ResetPasswordController;
use App\Http\Controllers\SearchController;
use App\Http\Controllers\TrainingController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\VerificationEmailController;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// Register
Route::group(['prefix' => 'register'], function () {
    Route::group(['middleware' => ['guest']], function () {
        Route::get('/', [RegisterController::class, 'index'])->name('register.index');
        Route::post('/', [RegisterController::class, 'save'])->name('register.save');
    });
});

// Login
Route::group(['prefix' => 'login'], function () {
    Route::group(['middleware' => ['guest']], function () {
        Route::get('/', [LoginController::class, 'index'])->name('login.index');
        Route::post('/', [LoginController::class, 'login'])->name('login');
    });
});

// Logout
Route::group(['prefix' => 'logout', 'middleware' => ['auth']], function () {
    Route::post('/', [LoginController::class, 'logout'])->name('logout');
});

Route::group(['middleware' => ['guest']], function () {
    // Verification
    Route::get('/verification/{code}', [VerificationEmailController::class, 'verifyEmail'])->name('verification');

    // Forgot Password
    Route::get('forgot', [ForgotPasswordController::class, 'index'])->name('forgot.index');
    Route::post('forgot', [ForgotPasswordController::class, 'sendTokenToEmail'])->name('token.send');

    // Reset and Update Password
    Route::get('token/{token}', [ResetPasswordController::class, 'index'])->name('token.index');
    Route::post('token/{token}', [ResetPasswordController::class, 'updatePassword'])->name('password.update');
});

// Profile
Route::group(['prefix' => 'users'], function () {
    Route::group(['middleware' => ['auth', 'admin.access']], function () {
        Route::get('/', [UserController::class, 'all'])->name('profile.all');
        Route::get('search', SearchController::class)->name('search');
    });

    //@TODO Сделать доступ для платных пользователей
    // Rate from admin panel
    Route::get('{user}/rate', function () {
        abort(404);
    });
    Route::put('{user}/rate', [RateController::class, 'updateRateFromAdmin'])->name('profile.rate.update');

    Route::group(['middleware' => ['auth', 'profile.access']], function () {
        Route::get('{user}', [UserController::class, 'show'])->name('profile.show');
        Route::get('{user}/edit', [UserController::class, 'edit'])->name('profile.edit');
        Route::put('{user}/edit', [UserController::class, 'update'])->name('profile.update');
        Route::delete('{user}', [UserController::class, 'delete'])->name('profile.delete');

        Route::get('{user}/ban', function () {
            abort(404);
        });
        Route::put('{user}/ban', [BanController::class, 'ban'])->name('profile.ban');
        Route::get('{user}/unban', function () {
            abort(404);
        });
        Route::put('{user}/unban', [BanController::class, 'unban'])->name('profile.unban');

        // Training Program
        Route::get('{user}/days', [TrainingController::class, 'startTraining'])->name('training.start');
        Route::get('{user}/days/{day}', [TrainingController::class, 'showTraining'])->name('training.show');
        Route::post('{user}/days/{day}', [TrainingController::class, 'openTraining'])->name('training.open');

        // Training Comments
        Route::get('{user}/days/{day}/comments', function () {
            abort(404);
        });
        Route::post('{user}/days/{day}/comments', [CommentController::class, 'save'])->name('comment.save');
    });
});

// Comments
Route::group(['prefix' => 'comments', 'middleware' => ['auth', 'admin.access']], function () {
    Route::get('/', [CommentController::class, 'all'])->name('comment.all');
});

Route::group(['prefix' => 'comments', 'middleware' => ['auth']], function () {
    Route::get('{comment}', function () {
        abort(404);
    });
    Route::delete('{comment}', [CommentController::class, 'delete'])->name('comment.delete');
});

// CKEditor
Route::get('ckeditor/image_upload', function () {
    abort(404);
});
Route::post('ckeditor/image_upload', [CKEditorController::class, 'upload'])->name('upload');

// Day
Route::group(['prefix' => 'days'], function () {
    Route::group(['middleware' => ['auth', 'admin.access']], function () {
        Route::get('/', [DayController::class, 'all'])->name('day.all');
        Route::get('create', [DayController::class, 'create'])->name('day.create');
        Route::post('create', [DayController::class, 'save'])->name('day.save');
        Route::get('{day}', [DayController::class, 'show'])->name('day.show');
        Route::get('{day}/edit', [DayController::class, 'edit'])->name('day.edit');
        Route::put('{day}/edit', [DayController::class, 'update'])->name('day.update');
        Route::delete('{day}/edit', [DayController::class, 'delete'])->name('day.delete');
    });
});

// Diary
Route::group(['prefix' => 'diaries'], function () {
    Route::group(['middleware' => ['auth']], function () {
        Route::get('create', [DiaryController::class, 'create'])->name('diary.create');
        Route::post('create', [DiaryController::class, 'save'])->name('diary.save');
    });

    Route::group(['middleware' => ['auth', 'diary.access']], function () {
        Route::get('{diary}/edit', [DiaryController::class, 'edit'])->name('diary.edit');
        Route::put('{diary}/edit', [DiaryController::class, 'update'])->name('diary.update');
        Route::delete('{diary}/edit', [DiaryController::class, 'delete'])->name('diary.delete');
    });
});

// Bonuses
Route::group(['prefix' => 'bonuses'], function () {
    Route::group(['middleware' => ['auth', 'admin.access']], function () {
        Route::get('create', [BonusController::class, 'create'])->name('bonus.create');
        Route::post('create', [BonusController::class, 'save'])->name('bonus.save');
        Route::get('{bonus}/edit', [BonusController::class, 'edit'])->name('bonus.edit');
        Route::put('{bonus}/edit', [BonusController::class, 'update'])->name('bonus.update');
        Route::delete('{bonus}/edit', [BonusController::class, 'delete'])->name('bonus.delete');
    });

    Route::group(['middleware' => ['auth', 'premium.access']], function () {
        Route::get('/', [BonusController::class, 'all'])->name('bonus.all');
        Route::get('{bonus}', [BonusController::class, 'show'])->name('bonus.show');
    });
});

// Rates
Route::group(['prefix' => 'rates'], function () {
    Route::get('/', [RateController::class, 'index'])->name('rate.index');

    Route::group(['middleware' => ['auth', 'admin.access']], function () {
        Route::get('create', [RateController::class, 'create'])->name('rate.create');
        Route::post('create', [RateController::class, 'save'])->name('rate.save');
        Route::get('{rate}/edit', [RateController::class, 'edit'])->name('rate.edit');
        Route::put('{rate}/edit', [RateController::class, 'update'])->name('rate.update');
        Route::delete('{rate}/edit', [RateController::class, 'delete'])->name('rate.delete');
    });

    Route::group(['middleware' => ['auth']], function () {
        // @TODO Сделать доступ для платных пользователей
        Route::get('{rate}', function () {
            abort(404);
        });
        Route::put('{rate}', [RateController::class, 'chooseAnotherRate'])->name('rate.choose');
    });
});

Route::get('/', function () {
    return view('pages.welcome');
})->name('home');

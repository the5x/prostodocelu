<section class="attention">
    <div class="wrapper">
        Вы используете бесплатный аккаунт, который ограничивает просмотр всего контента 14 дней
        → <a class="attention__link" href="{{ route('rate.index')  }}">Повысить тариф</a>
    </div>
</section>

@extends('layouts.app')

@section('keywords', '')
@section('description', '')

@section('content')
    <section class="header">
        <div class="wrapper">
            @include('includes.nav-primary')
        </div>

        <section class="hero">
            <div class="wrapper">
                <div class="trailer">
                    <div class="trailer__column">
                        <div class="trailer__circle pulse">
                            <img class="trailer__circle-image" src="{{ URL::to('/assets/images/ico-play.png') }}" alt="">
                        </div>
                    </div>
                    <div class="trailer__column">
                        <strong class="trailer__title">watch trailer</strong>
                    </div>
                </div>
                <h1 class="page__title">prosto do celu</h1>
                <h2 class="page__subtitle">Prosto do Celu — Schudnij w 100 dni</h2>
            </div>
        </section>

        <div class="header__image"><img class="header__image-pic" src="{{ URL::to('/assets/images/hero.png') }}" alt=""></div>
    </section>

    <section class="motivation">
        <div class="wrapper">
            <div class="motivation__box">
                <div class="motivation__column motivation__column_mw">
                    <span class="motivation__text"><span class="motivation__text motivation__text_color">Ты причина того</span>, как ты выглядишь и всего, Что с тобой происходит. Без оправданий! Без сожалений! <span class="motivation__text motivation__text_color">ТЫ — ПРИЧИНА ВСЕГО!</span></span>
                </div>
                <div class="motivation__column">
                    @guest
                    <a class="motivation__btn pulse__btn" href="{{ route('register.index') }}">Зарегистрироваться на курс</a><br/>
                    <span class="motivation__info">ЗА РЕГИСТРАЦИЮ <span class="motivation__info motivation__info_color">2 НЕДЕЛИ ПРОЕКТА БЕСПЛАТНО</span></span>
                    @endguest
                </div>
            </div>
        </div>
    </section>

    <section class="project">
        <div class="wrapper">
            <div class="project__box project__box_mb">
                <div class="project__column project__column_mx">
                    <h3 class="project__title">о проекте</h3>
                    <span class="project__subtitle">Эксклюзивный инвестиционный продукт: методология и особенности</span>
                </div>
                <div class="project__column">
                    <img class="project__image" src="{{ URL::to('/assets/images/about-pic-01.jpg') }}" alt="">
                    <div class="project__box">
                        <div class="project__column">
                            <p class="project__text project__text_mw">Поэтому управление брендом концентрирует стратегический маркетинг. Изменение глобальной стратегии настроено позитивно. Партисипативное планирование, согласно Ф.Котлеру, как всегда непредсказуемо.</p>
                        </div>
                        <div class="project__column">
                            <p class="project__text project__text_mw">Поэтому управление брендом концентрирует стратегический маркетинг. Изменение глобальной стратегии настроено позитивно. Партисипативное планирование, согласно Ф.Котлеру, как всегда непредсказуемо.</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="project__box">
                <div class="project__column project__column_mx">
                    <h3 class="project__title">О тренере</h3>
                    <span class="project__subtitle">Эксклюзивный инвестиционный продукт: методология и особенности</span>
                    <div class="project__cover">
                        <img class="project__image project__image_mt" src="{{ URL::to('/assets/images/about-pic-03.jpg') }}" alt="">
                    </div>
                    <strong class="project__strong project__strong_mx">Поэтому управление брендом концентрирует стратегический маркетинг.</strong>
                    <p class="project__text project__text_mw">Изменение глобальной стратегии настроено позитивно. Партисипативное планирование, согласно Ф.Котлеру, как всегда непредсказуемо.</p>
                </div>
                <div class="project__column">
                    <img class="project__image" src="{{ URL::to('/assets/images/about-pic-02.jpg') }}" alt="">
                    <div class="project__meta">
                        <div class="project__meta-column">
                            <p class="project__text">Поэтому управление брендом концентрирует стратегический маркетинг. Изменение глобальной стратегии настроено позитивно. Партисипативное планирование, согласно Ф.Котлеру, как всегда непредсказуемо.</p>
                        </div>
                        <div class="project__meta-column">
                            <div class="project__round" style="background-image: url( {{ URL::to('/assets/images/about-pic-04.jpg') }} )"></div>
                        </div>
                    </div>
                    <strong class="project__strong">Поэтому управление брендом концентрирует стратегический маркетинг</strong>
                    <p class="project__text">Изменение глобальной стратегии настроено позитивно. Партисипативное планирование, согласно Ф.Котлеру, как всегда непредсказуемо.</p>
                    <p class="project__text">Креатив основан на опыте. Conversion rate последовательно искажает презентационный материал. Взаимодействие корпорации и клиента существенно восстанавливает фирменный рекламный клаттер.</p>
                </div>
            </div>
        </div>
    </section>

    <footer class="footer-home">
        <div class="wrapper">
            <nav class="footer__nav">
                <ul class="footer__list">
                    @include('includes.nav-footer')
                </ul>
            </nav>

            <h3 class="page__title">prosto do celu</h3>
            <h4 class="page__subtitle">Prosto do Celu — Schudnij w 100 dni</h4>
        </div>
    </footer>

    <section class="copy">
        <a href="https://nastarte.by/" class="footer-home__link">Дизайн и разработка NaStarte</a>
    </section>


@stop

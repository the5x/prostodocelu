<?php

namespace Database\Factories;

use App\Models\Rate;
use Illuminate\Database\Eloquent\Factories\Factory;

class RateFactory extends Factory {

    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Rate::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition() {
        return [
            'price' => $this->faker->numberBetween(0, 50),
            'title' => $this->faker->realText($maxNbChars = 10),
            'subtitle' => $this->faker->realText($maxNbChars = 190),
            'advantage' => $this->faker->realText($maxNbChars = 500),
        ];
    }

}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('users', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('email')->unique();
            $table->string('password');
            $table->integer('weight')->default(0);
            $table->integer('height')->default(0);
            $table->integer('age')->default(0);
            $table->string('sex')->nullable();
            $table->string('purpose')->nullable();
            $table->string('photo')->nullable();
            $table->string('email_verify_code')->nullable();
            $table->string('is_admin')->default(0);
            $table->boolean('is_verified')->default(0);
            $table->boolean('is_banned')->default(0);
            $table->uuid('rate_id')->nullable();
            $table->rememberToken();

            $table->foreign('rate_id')->references('id')->on('rates')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('users');
    }

}

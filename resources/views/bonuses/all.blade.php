@extends('layouts.child')

@section('keywords', '')
@section('description', '')

@section('content')
    <section class="bonuses">
        <section class="wrapper">
            @if(auth()->check() && auth()->user()->is_admin)
                <a href="{{ route('bonus.create')  }}">
                    <img class="profile__ico-edit"
                         src="{{ URL::to('/assets/images/ico-edit.svg') }}" alt="">
                </a>
            @endif
            <h1 class="section__title">Бонусы</h1>
            <p class="bonuses__text">В соответствии с законом Ципфа, начальная стадия проведения исследования
                синхронизирует тактический имидж предприятия. Узнавание бренда ускоряет рекламный макет. Диверсификация
                бизнеса, суммируя приведенные примеры, экономит типичный традиционный канал.</p>

            <div class="bonuses__box">
                @isset($bonuses)
                    @foreach($bonuses as $bonus)
                        <div class="bonuses__column">
                            <img class="bonuses__iframe"
                                 src="https://img.youtube.com/vi/{{ $bonus->video }}/maxresdefault.jpg"
                                 alt="{{ $bonus->title }}">
                            <a class="bonuses__iframe-link"
                               href="{{ route('bonus.show', ['bonus' => $bonus->id])  }}">{{ Str::limit($bonus->title, 50, '...') }}</a>
                        </div>
                    @endforeach
                @endisset
            </div>
        </section>
    </section>
@stop

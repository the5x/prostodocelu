<?php

namespace App\Http\Controllers;

use App\Http\Requests\CommentRequest;
use App\Services\CommentService;
use App\Transformers\CommentTransformer;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;

class CommentController extends Controller {

    private CommentService $commentService;
    private CommentTransformer $commentTransformer;

    public function __construct(
        CommentService $commentService,
        CommentTransformer $commentTransformer
    ) {
        $this->commentService = $commentService;
        $this->commentTransformer = $commentTransformer;
    }

    public function save(CommentRequest $request, $profileId, $dayId): RedirectResponse {
        $data = $this->commentTransformer->transform($request->validated(), $profileId, $dayId);
        $this->commentService->save($data);

        return redirect()->back();
    }

    public function all(): View {
        $comments = $this->commentService->all();
        return view('comments.all', compact('comments'));
    }

    public function delete($id): RedirectResponse {
        $this->commentService->delete($id);

        return redirect()->back();
    }

}

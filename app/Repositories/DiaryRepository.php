<?php


namespace App\Repositories;

use App\Interfaces\DiaryRepositoryInterface;
use App\Models\Diary;

class DiaryRepository implements DiaryRepositoryInterface {

    private Diary $diaryModel;

    public function __construct(
        Diary $diaryModel
    ) {
        $this->diaryModel = $diaryModel;
    }

    public function create(array $data): Diary {
        return $this->diaryModel
            ->create($data);
    }

    public function findById(string $id): Diary {
        return $this->diaryModel
            ->findOrFail($id);
    }

    public function update(array $data, string $id): bool {
        return $this->diaryModel
            ->where('id', $id)
            ->update($data);
    }

    public function delete(string $id): bool {
        return $this->diaryModel
            ->where('id', $id)
            ->delete();
    }

}

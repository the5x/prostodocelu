<?php

namespace App\Services;


use App\Events\ForgotPasswordEvent;
use App\Models\PasswordReset;
use App\Repositories\AuthRepository;
use App\Transformers\PasswordTransformer;
use App\Transformers\TokenTransformer;
use Mail;

class AuthService {

    private AuthRepository $authRepository;
    private PasswordTransformer $passwordTransform;
    private TokenTransformer $tokenTransform;

    public function __construct(
        AuthRepository $authRepository,
        PasswordTransformer $passwordTransform,
        TokenTransformer $tokenTransform
    ) {
        $this->authRepository = $authRepository;
        $this->passwordTransform = $passwordTransform;
        $this->tokenTransform = $tokenTransform;
    }

    public function verifyEmail(string $code): bool {
        return $this->authRepository->verifyEmail($code);
    }

    public function generateToken(array $data): void {
        $tokenData = $this->tokenTransform->transform($data);
        $this->authRepository->generateToken($tokenData);

        event(new ForgotPasswordEvent($tokenData['email'], $tokenData['token']));
    }

    public function canUpdatePassword($data): ?PasswordReset {
        return $this->authRepository->canUpdatePassword($data);
    }

    public function updatePassword(array $data): bool {
        $updatedPassword = $this->passwordTransform->transform($data);
        return $this->authRepository->updatePassword($updatedPassword);
    }

    public function removeToken(string $email): bool {
        return $this->authRepository->removeToken($email);
    }

}

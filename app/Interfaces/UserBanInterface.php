<?php


namespace App\Interfaces;


interface UserBanInterface {

    public function ban(string $id): bool;

    public function unban(string $id): bool;

}

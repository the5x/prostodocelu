@extends('layouts.app')

@section('keywords', '')
@section('description', '')

@section('content')
    <section class="enter">
        <div class="wrapper">

            @include('includes.nav-primary')

            <div class="enter__form">
                <a class="enter__form-link enter__form-link_color" href="{{ route('login.index') }}">Вход</a>
                <span class="enter__form-link">Регистрация</span>

                <form action="{{ route('register.save') }}" method="post">
                    @csrf

                    @error('email')<div class="error">{{ $message }}</div>@enderror

                    <div class="enter__box">
                        <input value="{{ old('email') }}" name="email" placeholder="company@company.com" class="enter__input" type="email">
                    </div>

                    @error('password')<div class="error">{{ $message }}</div>@enderror

                    <div class="enter__box">
                    <input name="password" placeholder="password" class="enter__input" type="password">
                    </div>

                    <div class="enter__meta">
                        <div class="motivation__column motivation__column_mw">
                            <span class="motivation__text motivation__text_mb"><span class="motivation__text motivation__text_color">Ты причина того</span>, как ты выглядишь и всего, Что с тобой происходит. Без оправданий! Без сожалений! <span class="motivation__text motivation__text_color">ТЫ — ПРИЧИНА ВСЕГО!</span></span>
                        </div>
                        <div class="motivation__column">
                            <button class="motivation__btn pulse__btn" type="submit">Зарегистрироваться</button><br/>
                            <span class="motivation__info">ЗА РЕГИСТРАЦИЮ <span class="motivation__info motivation__info_color">2 НЕДЕЛИ ПРОЕКТА БЕСПЛАТНО</span></span>

                            <a class="forgot__link" href="{{ route('forgot.index')  }}">Забыли пароль?</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
@stop
